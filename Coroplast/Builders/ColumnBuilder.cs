﻿using Coroplast.Objects;
using System.Collections.Generic;
using System;
using Coroplast.Common;

namespace Coroplast.Builders
{
    public class ColumnBuilder
    {
        //-----

        private int _index;

        private readonly List<IColumnData> _columns;

        public ColumnBuilder()
        {
            _index = 0;
            _columns = new List<IColumnData>();
        }

        //-----

        public void Append(string name, string desc, string type)
        {
            _columns.Add(ColumnData.Create(_index, name, null, desc, type, null, ColumnVisibility.Default, OptionalType.None, false));
            _index++;
        }

        public void Append(string name, string alias, string desc, string type, ColumnVisibility visibility)
        {                      
            _columns.Add(ColumnData.Create(_index, name, alias, desc, type, null, visibility, OptionalType.None, false));
            _index++;
        }

        public void Append(string name, string alias, string desc, string type, object defaultValue, ColumnVisibility visibility)
        {
            _columns.Add(ColumnData.Create(_index, name, alias, desc, type, defaultValue, visibility, OptionalType.None, false));
            _index++;
        }

        public void Append(string name, string alias, string desc, string type, object defaultValue, ColumnVisibility visibility, OptionalType optional)
        {
            _columns.Add(ColumnData.Create(_index, name, alias, desc, type, defaultValue, visibility, optional, false));
            _index++;
        }

        public void Append(string name, string alias, string desc, string type, object defaultValue, ColumnVisibility visibility, OptionalType optional, bool random)
        {
            _columns.Add(ColumnData.Create(_index, name, alias, desc, type, defaultValue, visibility, optional, random));
            _index++;
        }

        //-----

        internal List<IColumnData> Build()
        {
            _index = 0;
            return _columns;
        }

        //-----
    }
}
