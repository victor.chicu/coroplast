﻿namespace Coroplast.Common
{
    public enum Rank
    {
        RECRUIT,       //CREATE/READ
        ADVANCED,      //CREATE/READ/UPDATE
        PROFESSIONAL,  //CREATE/READ/UPDATE/DELETE
    }
}
