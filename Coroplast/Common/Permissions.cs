﻿namespace Coroplast.Common
{
    public enum Permissions
    {
        NONE,
        CREATE,
        READ,
        UPDATE,
        DELETE
    }
}
