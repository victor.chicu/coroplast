﻿namespace Coroplast.Common
{
    public class DefaultSchemas
    {
        public const string ScanningSchemaName = "Scanning";
        public const string ChangeLogSchemaName = "ChangeLog";
        public const string DataStoreSchemaName = "DataStore";
        public const string EmployeesSchemaName = "Employees";
        public const string WarehouseSchemaName = "Warehouse";
        public const string PlanningStepsSchemaName = "PlanningSteps";    
        public const string ProductionControlSchemaName = "ProductionControl";
    }
}
