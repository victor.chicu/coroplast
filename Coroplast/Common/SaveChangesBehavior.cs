﻿using System;

namespace Coroplast.Common
{
    [Flags]
    public enum SaveChangesBehavior
    {
        None = 0,
        Save = 1,
        Edit = 2,
        Delete = 4,
    }
}