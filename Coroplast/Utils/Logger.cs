﻿using log4net;
using log4net.Config;
using System.Reflection;

namespace Coroplast.Utils
{
    public static class Logger
    {
        public static ILog Instance => _logger ?? (_logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType));

        private static ILog _logger;

        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }
    }
}
