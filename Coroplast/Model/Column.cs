﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Coroplast.Common;

namespace Coroplast.Model
{
    public class Column : INotifyPropertyChanged
    {
        public int Index { get; set; }

        public bool Randomly { get; set; }

        public string Type { get; set; }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }
        private string _title;

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }
        private string _description;

        public object DefaultValue
        {
            get
            {
                return _defaultValue;

            }
            set
            {
                _defaultValue = value;
                OnPropertyChanged();
            }
        }
        private object _defaultValue;

        public OptionalType Optional { get; set; }

        public ColumnVisibility ColumnVisibility { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
