﻿using System;
using System.Collections.Generic;

namespace Coroplast.Model
{
    public class Field
    {
        public Type Type { get; set; }

        public string Name { get; set; }

        public string Binding { get; set; }
    }

    public class Schema
    {
        public Schema()
        {
            Fields = new List<Field>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public List<Field> Fields { get; set; }
    }
}