﻿using System;
using System.Collections.Generic;
using Coroplast.Objects;
using MongoDB.Bson;

namespace Coroplast.Extensions
{
    public static class BsonDocumentExtension
    {
        public static List<object>[] ToRows(this IEnumerable<BsonDocument> documents, IList<IColumnData> columns)
        {
            var rows = new List<List<object>>();

            foreach (var document in documents)
            {
                var row = new List<object>();

                foreach (var column in columns)
                {
                    var type = Type.GetType(column.Type);

                    if (type == null)
                    {
                        throw new TypeLoadException($"Field type \"{column.Type}\" is not well formed.");
                    }

                    var typeCode = Type.GetTypeCode(type);

                    switch (typeCode)
                    {
                        case TypeCode.Empty:
                            {
                                row.Add(null);
                            }
                            break;
                        case TypeCode.Object:
                            {
                                if (type.IsAssignableFrom(typeof(Guid)))
                                {
                                    row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableGuid);
                                }
                            }
                            break;
                        case TypeCode.DBNull:
                            {
                                row.Add(null);
                            }
                            break;
                        case TypeCode.Boolean:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableBoolean);
                            }
                            break;
                        case TypeCode.Char:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.SByte:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Byte:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Int16:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.UInt16:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Int32:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.UInt32:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Int64:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt64);
                            }
                            break;
                        case TypeCode.UInt64:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt64);
                            }
                            break;
                        case TypeCode.Single:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableDouble);
                            }
                            break;
                        case TypeCode.Double:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableDouble);
                            }
                            break;
                        case TypeCode.Decimal:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableDecimal);
                            }
                            break;
                        case TypeCode.DateTime:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].ToNullableLocalTime());
                            }
                            break;
                        case TypeCode.String:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsString);
                            }
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                rows.Add(row);
            }

            return rows.ToArray();
        }
    }
}
