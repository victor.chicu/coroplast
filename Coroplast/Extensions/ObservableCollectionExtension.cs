﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Data;
using log4net;

namespace Coroplast.Extensions
{
    public static class ObservableCollectionExtension
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static async void AddRangeAsync(this ObservableCollection<List<object>> collection, List<object> objects)
        {
            var itemsLock = new object();

            BindingOperations.EnableCollectionSynchronization(collection, itemsLock);

            await Task.Factory.StartNew(() =>
                {
                    try
                    {
                        collection.Add(objects);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.ToString());
                    }
                });
        }
    }
}
