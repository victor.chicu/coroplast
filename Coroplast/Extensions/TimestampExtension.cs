﻿using System;

namespace Coroplast.Extensions
{
    public static class TimestampExtension
    {
        /// <summary>
        /// Converts a given DateTime into a Unix timestamp
        /// </summary>
        /// <param name="value">Any DateTime</param>
        /// <returns>The given DateTime in Unix timestamp format</returns>
        public static long AsTimestamp(this DateTime value)
        {
            return (long) Math.Truncate(value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
        }
    }
}
