﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;
using Coroplast.Builders;
using Coroplast.Common;
using Coroplast.Objects;
using Coroplast.Property;
using Coroplast.Storage;
using Coroplast.Storage.Repository;
using Coroplast.Storage.Settings;
using GalaSoft.MvvmLight;
using log4net;

namespace Coroplast.ViewModel
{
    public class EmployeesViewModel : ViewModelBase
    {
        //-----

        private readonly StorageManager<ISchemaData> _storageManager;

        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //-----

        public EmployeesViewModel()
        {
            try
            {
                if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                {
                    return;
                }

                DataTable = new DataTable();

                _storageManager = StorageManagerCache<ISchemaData>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.SchemasDataCollectionName));

                Schema = GetDefaultSchema(_storageManager, DefaultSchemas.EmployeesSchemaName, BuildColumns());

                var filteredColumns = Schema.Columns.Where(x => x.Equals(ColumnVisibility.Default));

                CreateColumns(filteredColumns);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        //-----

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { Set(() => SelectedIndex, ref _selectedIndex, value); }
        }
        private int _selectedIndex;

        public DataTable DataTable
        {
            get { return _dataTable; }
            set
            {
                _dataTable = value;
                Set(() => DataTable, ref _dataTable, value);
            }
        }
        private DataTable _dataTable;

        public ISchemaData Schema { get; }

        //-----

        public void NewRow(IEnumerable<IColumnData> columns, object[] row)
        {
            var newRow = DataTable.NewRow();

            var list = columns.Select(column => row[column.Index]).ToList();

            newRow.RowError = row[0].ToString();
            newRow.ItemArray = list.ToArray();

            DataTable.Rows.Add(newRow);
        }

        public void DeleteRow(int index)
        {
            DataTable.Rows.RemoveAt(index);
        }

        public DataRow GetRow(int index)
        {
            if (index < 0 || index > DataTable.Rows.Count) throw new IndexOutOfRangeException("There is no such row");

            var row = DataTable.Rows[index];

            var rowId = Guid.Parse(row.RowError);

            row.SetTag(rowId);

            return row;
        }

        //-----

        private void CreateColumns(IEnumerable<IColumnData> columns)
        {
            foreach (var column in columns)
            {
                var type = Type.GetType(column.Type);

                if (type == null)
                {
                    throw new TypeLoadException($"Column type \"{column.Type}\" is not well formed.");
                }

                DataTable.Columns.Add(column.Alias, type);
            }
        }

        private ISchemaData QuerySchema(string schemaName)
        {
            var repository = new SchemaRepository(_storageManager);

            using (var cursor = repository.GetSchema(schemaName))
            {
                if (!cursor.MoveNext() || cursor.Current == null || !cursor.Current.Any()) return null;

                var latestVersion = cursor.Current.Max(x => x.Version);

                return cursor.Current.First(x => x.Version.Equals(latestVersion));
            }
        }

        private ISchemaData GetDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = QuerySchema(schemaName);

            return schema ?? CreateDefaultSchema(storageManager, schemaName, columns);
        }

        //-----

        private static ISchemaData CreateDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = new SchemaData(schemaName, 1, columns);

            var repository = new SchemaRepository(storageManager);

            repository.CreateSchemaAsync(schema);

            return schema;
        }

        private static List<IColumnData> BuildColumns()
        {
            var columnBuilder = new ColumnBuilder();

            //System
            columnBuilder.Append("_id", "ID", "", typeof(Guid).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);
            columnBuilder.Append("_AT", "_AT", "", typeof(DateTime).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);

            //Default        
            columnBuilder.Append(EmployeesColumns.Name, EmployeesColumns.Name, $"Enter \"{EmployeesColumns.Name}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(EmployeesColumns.Surname, EmployeesColumns.Surname, $"Enter \"{EmployeesColumns.Surname}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(EmployeesColumns.PhoneNumber, EmployeesColumns.PhoneNumber, $"Enter \"{EmployeesColumns.PhoneNumber}\" here", typeof(string).FullName, ColumnVisibility.Default);      
            columnBuilder.Append(EmployeesColumns.OperatorNumber, EmployeesColumns.OperatorNumber, $"Enter \"{EmployeesColumns.OperatorNumber}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(EmployeesColumns.Shift, EmployeesColumns.Shift, $"Enter \"{EmployeesColumns.Shift}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(EmployeesColumns.Group, EmployeesColumns.Group, $"Enter \"{EmployeesColumns.Group}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(EmployeesColumns.TeamGroup, EmployeesColumns.TeamGroup, $"Enter \"{EmployeesColumns.TeamGroup}\" here", typeof(string).FullName, ColumnVisibility.Default);

            return columnBuilder.Build();
        }

        //-----       
    }
}
