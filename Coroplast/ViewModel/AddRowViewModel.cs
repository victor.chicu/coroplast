﻿using Coroplast.Objects;
using GalaSoft.MvvmLight;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Coroplast.Model;
using System;
using System.ComponentModel;
using System.Globalization;
using GalaSoft.MvvmLight.Command;
using System.Windows;
using System.Linq;
using Coroplast.Common;
using Coroplast.Extensions;

namespace Coroplast.ViewModel
{
    public class AddRowViewModel : ViewModelBase
    {
        //-----

        private ObservableCollection<Column> _addedColumns;

        private readonly List<object> _addedRow = new List<object>();
        private readonly List<Column> _columns;
        private readonly Dictionary<string, bool> _validationErrors;

        private RelayCommand<Window> _addCommand;


        //-----

        public AddRowViewModel(IEnumerable<IColumnData> columns)
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                return;
            }

            _columns = new List<Column>();
            _addCommand = new RelayCommand<Window>(OnAddCommand, TryAddCommand);
            _addedColumns = new ObservableCollection<Column>();

            _validationErrors = new Dictionary<string, bool>();

            foreach (var column in columns)
            {
                switch (column.ColumnVisibility)
                {
                    case ColumnVisibility.None:
                        break;

                    case ColumnVisibility.Hidden:
                        {
                            var newColumn = CreateColumn(column);
                            _columns.Add(newColumn);
                        }
                        break;

                    case ColumnVisibility.Default:
                        {
                            var newColumn = CreateColumn(column, true);

                            _columns.Add(newColumn);

                            //Random default columns will be shown on edit
                            if (!column.Random) AddedColumns.Add(newColumn);
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        //-----

        public RelayCommand<Window> AddCommand
        {
            get { return _addCommand; }
            set { Set(() => AddCommand, ref _addCommand, value); }
        }

        public ObservableCollection<Column> AddedColumns
        {
            get { return _addedColumns; }
            set { Set(() => AddedColumns, ref _addedColumns, value); }
        }

        //-----


        private void OnAddCommand(Window window)
        {
            foreach (var column in _columns)
            {
                var type = Type.GetType(column.Type);

                if (type == null)
                {
                    throw new TypeLoadException($"Field type \"{column.Type}\" is not well formed.");
                }

                var typeCode = Type.GetTypeCode(type);

                switch (typeCode)
                {
                    case TypeCode.Empty:
                        {
                            _addedRow.Add(string.Empty);
                        }
                        break;
                    case TypeCode.Object:
                        if (type.IsAssignableFrom(typeof(Guid)))
                        {
                            _addedRow.Add(column.Randomly ? Guid.NewGuid() : column.DefaultValue);
                        }
                        else
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.DBNull:
                        {
                            _addedRow.Add(DBNull.Value);
                        }
                        break;
                    case TypeCode.Boolean:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Char:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.SByte:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Byte:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Int16:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.UInt16:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Int32:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.UInt32:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Int64:
                        {
                            switch (column.Optional)
                            {
                                case OptionalType.None:
                                    {
                                        _addedRow.Add(column.DefaultValue);
                                    }
                                    break;
                                case OptionalType.Timestamp:
                                    {
                                        _addedRow.Add(column.Randomly ? DateTime.Now.AsTimestamp() : column.DefaultValue);
                                    }
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                        }
                        break;
                    case TypeCode.UInt64:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Single:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Double:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Decimal:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.DateTime:
                        {
                            if (column.Randomly)
                            {
                                _addedRow.Add(DateTime.Now);
                            }
                            else
                            {
                                if (column.DefaultValue != null)
                                {
                                    var formats = new[]
                                        {
                                            "dd/MM/yyyy",
                                            "dd.MM.yyyy",
                                            "dd/MM/yyyy HH:mm:ss",
                                            "dd.MM.yyyy HH:mm:ss"
                                        };

                                    var provider = CultureInfo.InvariantCulture;

                                    try
                                    {
                                        _addedRow.Add(DateTime.ParseExact(column.DefaultValue.ToString(), formats, provider, DateTimeStyles.AssumeLocal));
                                    }
                                    catch (FormatException)
                                    {
                                        _addedRow.Add(DBNull.Value);
                                    }
                                }
                                else
                                {
                                    _addedRow.Add(column.DefaultValue);
                                }
                            }
                        }
                        break;
                    case TypeCode.String:
                        {
                            _addedRow.Add(column.DefaultValue);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            window?.Close();
        }

        private void CellPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var cell = (Column) sender;

            var type = Type.GetType(cell.Type);

            if (type == null) throw new TypeLoadException(cell.Type);

            if (type.IsAssignableFrom(typeof(int)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    int value;

                    if (!int.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is an integer type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(long)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    long value;

                    if (!long.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a long type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(Guid)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    Guid value;

                    if (!Guid.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is an id type");
                    }

                    if (value.Equals(Guid.Empty))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be empty id");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(float)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    float value;

                    if (!float.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a float type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(double)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    double value;

                    if (!double.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a double type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(decimal)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    decimal value;

                    if (!decimal.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a decimal type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(DateTime)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (isEmpty)
                {
                    cell.DefaultValue = null;
                }
                else
                {
                    var defaultValue = cell.DefaultValue.ToString();

                    if (defaultValue.StartsWith("-"))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a date time type");
                    }

                    var formats = new[]
                        {
                            "dd/MM/yyyy",
                            "dd.MM.yyyy",
                            "dd/MM/yyyy HH:mm:ss",
                            "dd.MM.yyyy HH:mm:ss"
                        };

                    var provider = CultureInfo.InvariantCulture;

                    try
                    {
                        DateTime.ParseExact(defaultValue, formats, provider, DateTimeStyles.AssumeLocal);
                    }
                    catch (FormatException)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a date time type");
                    }
                }
            }

            _validationErrors[cell.Title] = false;
        }

        private bool TryAddCommand(Window window)
        {
            return !_validationErrors.Any(x => x.Value);
        }

        private Column CreateColumn(IColumnData column, bool withEvent = false)
        {
            var newColumn = new Column
                {
                    Type = column.Type,
                    Title = column.Alias,
                    Index = column.Index,
                    Randomly = column.Random,
                    Optional = column.Optional,
                    Description = column.Description,
                    DefaultValue = column.DefaultValue,
                    ColumnVisibility = column.ColumnVisibility,
                };

            if (withEvent)
            {
                newColumn.PropertyChanged += CellPropertyChanged;
            }

            return newColumn;
        }

        //-----

        internal object[] GetRow()
        {
            return _addedRow.ToArray();
        }

        //-----
    }
}