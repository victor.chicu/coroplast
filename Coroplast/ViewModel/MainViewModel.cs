using GalaSoft.MvvmLight;
using System;
using System.Collections.Concurrent;
using System.Windows;
using System.ComponentModel;
using GalaSoft.MvvmLight.Command;
using log4net;
using System.Reflection;
using Coroplast.View;
using Coroplast.Storage;
using Coroplast.Objects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Coroplast.Common;
using Coroplast.Property;
using Coroplast.Storage.Repository;
using Coroplast.Storage.Settings;
using iTextSharp.text;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Win32;
using MongoDB.Bson;
using MongoDB.Driver;
using Coroplast.XLSX;
using Coroplast.Pdf;

namespace Coroplast.ViewModel
{
    //todo: REFACTORING! MANY CODE DUPLICATION..

    public class MainViewModel : ViewModelBase
    {
        //-----

        private int _tabControlSelectedIndex;

        private bool _changeLogVisibility;
        private bool _scanningVisibility;
        private bool _sleepControlVisibility;

        private string _queryString;
        private string _lastLoggedAction;

        // ReSharper disable once UnusedMember.Local
        private const int DEFAULT_DELAY_TIME = 500;

        private RelayCommand _queryButtonCommand;
        private RelayCommand _addRowButtonCommand;
        private RelayCommand _editRowButtonCommand;
        private RelayCommand _scanningButtonCommand; 
        private RelayCommand _changeLogButtonCommand;
        private RelayCommand _deleteRowButtonCommand;
        private RelayCommand _xlsImportButtonCommand;
        private RelayCommand _xlsExportButtonCommand;
        private RelayCommand _pdfExportButtonCommand;
        private RelayCommand _saveChangesButtonCommand;
        private RelayCommand _synchronizeButtonCommand;
        private RelayCommand _openSettingsButtonCommand;
        private RelayCommand<KeyEventArgs> _queryStringTokenizedTextBox;
        private RelayCommand<DataGridRowEventArgs> _dataGridLoadingRowCommand;
        private RelayCommand<MouseButtonEventArgs> _dataGridMouseDoubleClickCommand;
        private RelayCommand<SelectionChangedEventArgs> _dataGridSelectionChangedCommand;

        private readonly string[] _tokenizers = {
                "AND",
                "OR",
                "=",
                ">",
                "<",
                "<>",
                ">=",
                "<=",
                "LIKE"
            };

        private readonly StorageManager<BsonDocument> _dataStorage;
        private readonly StorageManager<BsonDocument> _planningStorage;
        private readonly StorageManager<BsonDocument> _changeLogStorage;  
        private readonly StorageManager<BsonDocument> _employeesStorage;
        private readonly StorageManager<BsonDocument> _warehouseStorage;
        private readonly StorageManager<BsonDocument> _productionStorage;

        private readonly Dictionary<string, ISchemaData> _schemaCache;

        private ObservableCollection<string> _queryTokenizers;

        // ReSharper disable once InconsistentNaming
        private const int MAX_CONCURRENT_STACK = 5;

        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //-----

        public MainViewModel()
        {
            try
            {
                //todo: Before to run application check backup on google disk

#if DEBUG
                if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                {
                    return;
                }
#endif

                TestConnection();

                StorageManager.RegisterWellKnownTypes();

                AddedDocuments = new ConcurrentStack<KeyValuePair<Guid, BsonDocument>>[MAX_CONCURRENT_STACK];
                EditedDocuments = new ConcurrentStack<KeyValuePair<Guid, BsonDocument>>[MAX_CONCURRENT_STACK];
                DeletedDocuments = new ConcurrentStack<KeyValuePair<Guid, BsonDocument>>[MAX_CONCURRENT_STACK];

                for (var i = 0; i < MAX_CONCURRENT_STACK; i++)
                {
                    AddedDocuments[i] = new ConcurrentStack<KeyValuePair<Guid, BsonDocument>>();
                    EditedDocuments[i] = new ConcurrentStack<KeyValuePair<Guid, BsonDocument>>();
                    DeletedDocuments[i] = new ConcurrentStack<KeyValuePair<Guid, BsonDocument>>();
                }

                _queryButtonCommand = new RelayCommand(OnQueryButtonCommand);
                _addRowButtonCommand = new RelayCommand(OnAddRowButtonCommand);
                _editRowButtonCommand = new RelayCommand(OnEditRowButtonCommand);
                _scanningButtonCommand = new RelayCommand(OnScanningButtonCommand);
                _changeLogButtonCommand = new RelayCommand(OnChangeLogButtonCommand);
                _deleteRowButtonCommand = new RelayCommand(OnDeleteRowButtonCommand);

                _xlsImportButtonCommand = new RelayCommand(OnXlsImportButtonCommand);           
                _xlsExportButtonCommand = new RelayCommand(OnXlsExportButtonCommand);

                _pdfExportButtonCommand = new RelayCommand(OnPdfExportButtonCommand);
                _synchronizeButtonCommand = new RelayCommand(OnSynchronizeCommand);
                _saveChangesButtonCommand = new RelayCommand(OnSaveChagensButtonCommand);
                _openSettingsButtonCommand = new RelayCommand(OnOpenSettingsButtonCommand);
                _queryStringTokenizedTextBox = new RelayCommand<KeyEventArgs>(OnQueryStringTokenizedTextBoxCommand);

                _dataGridLoadingRowCommand = new RelayCommand<DataGridRowEventArgs>(OnDataGridLoadingRowCommand);
                _dataGridMouseDoubleClickCommand = new RelayCommand<MouseButtonEventArgs>(OnDataGridDoubleClickCommand);
                _dataGridSelectionChangedCommand = new RelayCommand<SelectionChangedEventArgs>(OnDataGridSelectionChangedCommand);

                _dataStorage = StorageManagerCache<BsonDocument>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.DataStoreCollectionName));
                _planningStorage = StorageManagerCache<BsonDocument>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.PlanningDataCollectonName));
                _changeLogStorage = StorageManagerCache<BsonDocument>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.ChangeLogDataCollectionName));
                _employeesStorage = StorageManagerCache<BsonDocument>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.EmployeesDataCollectionName));
                _warehouseStorage = StorageManagerCache<BsonDocument>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.WarehouseDataCollectionName));
                _productionStorage = StorageManagerCache<BsonDocument>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.ProductionControlCollectionName));

                _schemaCache = new Dictionary<string, ISchemaData>();

                InitializeSchemaCaches();
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
                Environment.Exit(1);
            }
        }

        //-----
      
        public bool ScanningVisibility
        {
            get { return _scanningVisibility; }
            set { Set(() => ScanningVisibility, ref _scanningVisibility, value); }
        }

        public bool ChangeLogVisibility
        {
            get { return _changeLogVisibility; }
            set { Set(() => ChangeLogVisibility, ref _changeLogVisibility, value); }
        }

        public bool SleepControlVisibility
        {
            get { return _sleepControlVisibility; }
            set { Set(() => SleepControlVisibility, ref _sleepControlVisibility, value); }
        }

        public string QueryString
        {
            get { return _queryString; }
            set { Set(() => QueryString, ref _queryString, value); }
        }

        public ObservableCollection<string> QueryTokenizers
        {
            get { return _queryTokenizers; }
            set { Set(() => QueryTokenizers, ref _queryTokenizers, value); }
        }

        public ConcurrentStack<KeyValuePair<Guid, BsonDocument>>[] AddedDocuments { get; set; }
        public ConcurrentStack<KeyValuePair<Guid, BsonDocument>>[] EditedDocuments { get; set; }
        public ConcurrentStack<KeyValuePair<Guid, BsonDocument>>[] DeletedDocuments { get; set; }

        //-----

        public int TabControlSelectedIndex
        {
            get
            {
                var category = (DataTableCategory) _tabControlSelectedIndex;
                OnTabControlSelectedIndexCommand(category);
                return _tabControlSelectedIndex;
            }
            set { Set(() => TabControlSelectedIndex, ref _tabControlSelectedIndex, value); }
        }

        public string LastLoggedAction
        {
            get { return _lastLoggedAction; }
            set { Set(() => LastLoggedAction, ref _lastLoggedAction, value); }
        }

        public RelayCommand QueryButtonCommand
        {
            get { return _queryButtonCommand; }
            set { Set(() => QueryButtonCommand, ref _queryButtonCommand, value); }
        }

        public RelayCommand AddRowButtonCommand
        {
            get { return _addRowButtonCommand; }
            set { Set(() => AddRowButtonCommand, ref _addRowButtonCommand, value); }
        }

        public RelayCommand EditRowButtonCommand
        {
            get { return _editRowButtonCommand; }
            set { Set(() => EditRowButtonCommand, ref _editRowButtonCommand, value); }
        }

        public RelayCommand ChangeLogButtonCommand
        {
            get { return _changeLogButtonCommand; }
            set { Set(() => ChangeLogButtonCommand, ref _changeLogButtonCommand, value); }
        }

        public RelayCommand ScanningButtonCommand
        {
            get { return _scanningButtonCommand; }
            set { Set(() => ScanningButtonCommand, ref _scanningButtonCommand, value); }
        }

        public RelayCommand DeleteRowButtonCommand
        {
            get { return _deleteRowButtonCommand; }
            set { Set(() => DeleteRowButtonCommand, ref _deleteRowButtonCommand, value); }
        }

        public RelayCommand XlsImportButtonCommand
        {
            get { return _xlsImportButtonCommand; }
            set { Set(() => XlsImportButtonCommand, ref _xlsImportButtonCommand, value); }
        }

        public RelayCommand XlsExportButtonCommand
        {
            get { return _xlsExportButtonCommand; }
            set { Set(() => XlsExportButtonCommand, ref _xlsExportButtonCommand, value); }
        }

        public RelayCommand PdfExportButtonCommand
        {
            get { return _pdfExportButtonCommand; }
            set { Set(() => PdfExportButtonCommand, ref _pdfExportButtonCommand, value); }
        }

        public RelayCommand SaveChangesButtonCommand
        {
            get { return _saveChangesButtonCommand; }
            set { Set(() => SaveChangesButtonCommand, ref _saveChangesButtonCommand, value); }
        }

        public RelayCommand SynchronizeButtonCommand
        {
            get { return _synchronizeButtonCommand; }
            set { Set(() => SynchronizeButtonCommand, ref _synchronizeButtonCommand, value); }
        }

        public RelayCommand OpenSettingsButtonCommand
        {
            get { return _openSettingsButtonCommand; }
            set { Set(() => OpenSettingsButtonCommand, ref _openSettingsButtonCommand, value); }
        }


        public RelayCommand<KeyEventArgs> QueryStringTokenizedTextBox
        {
            get { return _queryStringTokenizedTextBox; }
            set { Set(() => QueryStringTokenizedTextBox, ref _queryStringTokenizedTextBox, value); }
        }

        public RelayCommand<DataGridRowEventArgs> DataGridLoadingRowCommand
        {
            get { return _dataGridLoadingRowCommand; }
            set { Set(() => DataGridLoadingRowCommand, ref _dataGridLoadingRowCommand, value); }
        }

        public RelayCommand<MouseButtonEventArgs> DataGridMouseDoubleClickCommand
        {
            get { return _dataGridMouseDoubleClickCommand; }
            set { Set(() => DataGridMouseDoubleClickCommand, ref _dataGridMouseDoubleClickCommand, value); }
        }

        public RelayCommand<SelectionChangedEventArgs> DataGridSelectionChangedCommand
        {
            get { return _dataGridSelectionChangedCommand; }
            set { Set(() => DataGridSelectionChangedCommand, ref _dataGridSelectionChangedCommand, value); }
        }

        //-----

        private void OnQueryButtonCommand()
        {
            try
            {
                var category = (DataTableCategory) _tabControlSelectedIndex;

                switch (category)
                {
                    case DataTableCategory.DataStore:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<DataStoreViewModel>();
                            viewModel.DataTable.DefaultView.RowFilter = _queryString;
                        }
                        break;

                    case DataTableCategory.Employees:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<EmployeesViewModel>();
                            viewModel.DataTable.DefaultView.RowFilter = _queryString;
                        }
                        break;

                    case DataTableCategory.Warehouse:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();
                            viewModel.DataTable.DefaultView.RowFilter = _queryString;
                        }
                        break;

                    case DataTableCategory.PlanningSteps:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();
                            viewModel.DataTable.DefaultView.RowFilter = _queryString;
                        }
                        break;

                    case DataTableCategory.ProudctionControl:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();
                            viewModel.DataTable.DefaultView.RowFilter = _queryString;
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if  DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnSynchronizeCommand()
        {
            //throw new NotImplementedException();
        }

        private void OnAddRowButtonCommand()
        {
            try
            {
                AddRowViewModel addRowViewModel;

                var category = (DataTableCategory) _tabControlSelectedIndex;

                switch (category)
                {
                    case DataTableCategory.DataStore:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<DataStoreViewModel>();

                            var columns = viewModel.Schema.Columns;

                            addRowViewModel = ShowAddRowDialogWindow(columns);

                            var row = addRowViewModel.GetRow();

                            if (row.Length > 0)
                            {
                                AddNewVirtualRow(row, columns);
                                viewModel.NewRow(columns.Where(x => x.Equals(ColumnVisibility.Default)), row);
                            }
                        }
                        break;

                    case DataTableCategory.Employees:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<EmployeesViewModel>();

                            var columns = viewModel.Schema.Columns;

                            addRowViewModel = ShowAddRowDialogWindow(columns);

                            var row = addRowViewModel.GetRow();

                            if (row.Length > 0)
                            {
                                AddNewVirtualRow(row, columns);

                                viewModel.NewRow(columns.Where(x => x.Equals(ColumnVisibility.Default)), row);
                            }
                        }
                        break;

                    case DataTableCategory.Warehouse:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();

                            var columns = viewModel.Schema.Columns;

                            addRowViewModel = ShowAddRowDialogWindow(columns);

                            var row = addRowViewModel.GetRow();

                            if (row.Length > 0)
                            {
                                AddNewVirtualRow(row, columns);

                                viewModel.NewRow(columns.Where(x => x.Equals(ColumnVisibility.Default)), row);
                            }
                        }
                        break;

                    case DataTableCategory.PlanningSteps:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();

                            var columns = viewModel.Schema.Columns;

                            addRowViewModel = ShowAddRowDialogWindow(columns);

                            var row = addRowViewModel.GetRow();

                            if (row.Length > 0)
                            {
                                AddNewVirtualRow(row, columns);

                                viewModel.NewRow(columns.Where(x => x.Equals(ColumnVisibility.Default)), row);
                            }
                        }
                        break;

                    case DataTableCategory.ProudctionControl:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();

                            var columns = viewModel.Schema.Columns;

                            addRowViewModel = ShowAddRowDialogWindow(columns);

                            var row = addRowViewModel.GetRow();

                            if (row.Length > 0)
                            {
                                AddNewVirtualRow(row, columns);

                                viewModel.NewRow(columns.Where(x => x.Equals(ColumnVisibility.Default)), row);
                            }
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if  DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnEditRowButtonCommand()
        {
            try
            {
                var category = (DataTableCategory) _tabControlSelectedIndex;

                switch (category)
                {
                    case DataTableCategory.DataStore:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<DataStoreViewModel>();

                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            var schema = viewModel.Schema;
                            var selectedIndex = viewModel.SelectedIndex;
                            var filteredColumns = schema.Columns.Where(x => x.Equals(ColumnVisibility.Default)).ToArray();

                            var editRowViewModel = OpenEditRowDialogWindow(filteredColumns, row.ItemArray);

                            var editedRow = editRowViewModel.GetRow();

                            if (editedRow.Length > 0)
                            {
                                for (var i = 0; i < editedRow.Length; i++)
                                {
                                    viewModel.DataTable.Rows[selectedIndex][filteredColumns[i].Alias] = editedRow[i];
                                }

                                NewEditedVirtualRow(row, filteredColumns, editedRow);
                            }
                        }
                        break;

                    case DataTableCategory.Employees:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<EmployeesViewModel>();

                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            var schema = viewModel.Schema;
                            var selectedIndex = viewModel.SelectedIndex;
                            var filteredColumns = schema.Columns.Where(x => x.Equals(ColumnVisibility.Default)).ToArray();

                            var editRowViewModel = OpenEditRowDialogWindow(filteredColumns, row.ItemArray);

                            var editedRow = editRowViewModel.GetRow();

                            if (editedRow.Length > 0)
                            {
                                for (var i = 0; i < editedRow.Length; i++)
                                {
                                    viewModel.DataTable.Rows[selectedIndex][filteredColumns[i].Alias] = editedRow[i];
                                }

                                NewEditedVirtualRow(row, filteredColumns, editedRow);
                            }
                        }
                        break;

                    case DataTableCategory.Warehouse:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();

                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            var schema = viewModel.Schema;
                            var itemArray = row.ItemArray;
                            var selectedIndex = viewModel.SelectedIndex;
                            var filteredColumns = schema.Columns.Where(x => x.Equals(ColumnVisibility.Default)).ToArray();

                            var editRowViewModel = OpenEditRowDialogWindow(filteredColumns, itemArray);

                            var editedRow = editRowViewModel.GetRow();

                            if (editedRow.Length > 0)
                            {
                                for (var i = 0; i < editedRow.Length; i++)
                                {
                                    viewModel.DataTable.Rows[selectedIndex][filteredColumns[i].Alias] = editedRow[i];
                                }

                                NewEditedVirtualRow(row, filteredColumns, editedRow);

                                var rowId = Guid.Parse(viewModel.DataTable.Rows[selectedIndex].RowError);

                                InsertChangeLogAsync(rowId, itemArray);
                            }
                        }
                        break;

                    case DataTableCategory.PlanningSteps:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();

                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            var schema = viewModel.Schema;
                            var selectedIndex = viewModel.SelectedIndex;
                            var filteredColumns = schema.Columns.Where(x => x.Equals(ColumnVisibility.Default)).ToArray();

                            var editRowViewModel = OpenEditRowDialogWindow(filteredColumns, row.ItemArray);

                            var editedRow = editRowViewModel.GetRow();

                            if (editedRow.Length > 0)
                            {
                                for (var i = 0; i < editedRow.Length; i++)
                                {
                                    viewModel.DataTable.Rows[selectedIndex][filteredColumns[i].Alias] = editedRow[i];
                                }

                                NewEditedVirtualRow(row, filteredColumns, editedRow);
                            }
                        }
                        break;

                    case DataTableCategory.ProudctionControl:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();

                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            var schema = viewModel.Schema;
                            var selectedIndex = viewModel.SelectedIndex;
                            var filteredColumns = schema.Columns.Where(x => x.Equals(ColumnVisibility.Default)).ToArray();

                            var editRowViewModel = OpenEditRowDialogWindow(filteredColumns, row.ItemArray);

                            var editedRow = editRowViewModel.GetRow();

                            if (editedRow.Length > 0)
                            {
                                for (var i = 0; i < editedRow.Length; i++)
                                {
                                    viewModel.DataTable.Rows[selectedIndex][filteredColumns[i].Alias] = editedRow[i];
                                }

                                NewEditedVirtualRow(row, filteredColumns, editedRow);
                            }
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnScanningButtonCommand()
        {
            try
            {
                var scanningWindow = new ScanningWindow
                    {
                        Title = "Scanning barcodes window",
                        Width = 800,
                        Height = 600,
                        ResizeMode = ResizeMode.NoResize,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen
                    };
             
                var result = scanningWindow.ShowDialog();

                if (!result.HasValue)
                {
                    throw new Exception("Open scanning barcodes window fail");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnChangeLogButtonCommand()
        {
            try
            {
                var changeLogViewModel = ServiceLocator.Current.GetInstance<ChangeLogViewModel>();

                var changeLogWindow = new ChangeLogWindow
                    {
                        Title = "Change log window",
                        Width = 800,
                        Height = 600,
                        ResizeMode = ResizeMode.CanResize,
                        DataContext = changeLogViewModel,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    };

                var warehouseViewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();

                changeLogViewModel.LoadChangeLogAsync(warehouseViewModel.DataTable.Rows[warehouseViewModel.SelectedIndex]);

                var result = changeLogWindow.ShowDialog();

                if (!result.HasValue)
                {
                    throw new Exception("Open change log window fail");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnDeleteRowButtonCommand()
        {
            try
            {
                var category = (DataTableCategory) _tabControlSelectedIndex;

                switch (category)
                {
                    case DataTableCategory.DataStore:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<DataStoreViewModel>();
                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            viewModel.DeleteRow(viewModel.SelectedIndex);

                            NewDeletedVirtualRow(row);
                        }
                        break;

                    case DataTableCategory.Employees:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<EmployeesViewModel>();
                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            viewModel.DeleteRow(viewModel.SelectedIndex);
                            NewDeletedVirtualRow(row);
                        }
                        break;

                    case DataTableCategory.Warehouse:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();
                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            viewModel.DeleteRow(viewModel.SelectedIndex);
                            NewDeletedVirtualRow(row);
                        }
                        break;

                    case DataTableCategory.PlanningSteps:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();
                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            viewModel.DeleteRow(viewModel.SelectedIndex);
                            NewDeletedVirtualRow(row);
                        }
                        break;

                    case DataTableCategory.ProudctionControl:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();
                            var row = viewModel.GetRow(viewModel.SelectedIndex);
                            viewModel.DeleteRow(viewModel.SelectedIndex);
                            NewDeletedVirtualRow(row);
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnXlsImportButtonCommand()
        {
            try
            {
                if (!AddedDocuments[_tabControlSelectedIndex].IsEmpty)
                {
                    MessageBox.Show(string.Format("You have added \"{0}\" row(s). Save all changes before to export xls file.", AddedDocuments[_tabControlSelectedIndex].Count));
                    return;
                }

                if (!EditedDocuments[_tabControlSelectedIndex].IsEmpty)
                {
                    MessageBox.Show(string.Format("You have edited \"{0}\" row(s). Save all changes before to export xls file.", EditedDocuments[_tabControlSelectedIndex].Count));
                    return;
                }

                if (!DeletedDocuments[_tabControlSelectedIndex].IsEmpty)
                {
                    MessageBox.Show(string.Format("You have deleted \"{0}\" row(s). Save all changes before to export xls file.", DeletedDocuments[_tabControlSelectedIndex].Count));
                    return;
                }

                var openFileDialog = new OpenFileDialog
                {
                    Filter = "XLSX documents (.xlsx)|*.xlsx"
                };

                var result = openFileDialog.ShowDialog();

                if (result.HasValue && !result.Value) return;
                
                var fileName = openFileDialog.FileName;

                SleepControlVisibility = true;

                ExecuteTask(() =>
                {
                    XlsManager xlsManager = new XlsManager(fileName);

                    var category = (DataTableCategory)_tabControlSelectedIndex;

                    switch (category)
                    {
                        case DataTableCategory.DataStore:
                            {
                                var viewModel = ServiceLocator.Current.GetInstance<DataStoreViewModel>();

                                var columns = viewModel.Schema.Columns.ToArray();

                                List<object[]> rows = xlsManager.Import(columns);

                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    try
                                    {
                                        var defaultColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

                                        foreach (var row in rows)
                                        {
                                            viewModel.NewRow(defaultColumns, row);
                                            var rowId = row[0] as Guid?;
                                            var document = ToBsonDocument(columns, row.ToArray());
                                            AddedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId.Value, document));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex.ToString());
#if DEBUG
                                        MessageBox.Show(ex.ToString());
#endif
                                    }
                                }));
                            }
                            break;

                        case DataTableCategory.Employees:
                            {
                                var viewModel = ServiceLocator.Current.GetInstance<EmployeesViewModel>();

                                var columns = viewModel.Schema.Columns.ToArray();

                                List<object[]> rows = xlsManager.Import(columns);

                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    try
                                    {
                                        var defaultColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

                                        foreach (var row in rows)
                                        {
                                            viewModel.NewRow(defaultColumns, row);
                                            var rowId = row[0] as Guid?;
                                            var document = ToBsonDocument(columns, row.ToArray());
                                            AddedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId.Value, document));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex.ToString());
#if DEBUG
                                        MessageBox.Show(ex.ToString());
#endif
                                    }
                                }));
                            }
                            break;

                        case DataTableCategory.Warehouse:
                            {
                                var viewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();

                                var columns = viewModel.Schema.Columns.ToArray();

                                List<object[]> rows = xlsManager.Import(columns);

                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    try
                                    {
                                        var defaultColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

                                        foreach (var row in rows)
                                        {
                                            viewModel.NewRow(defaultColumns, row);
                                            var rowId = row[0] as Guid?;
                                            var document = ToBsonDocument(columns, row.ToArray());
                                            AddedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId.Value, document));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex.ToString());
#if DEBUG
                                        MessageBox.Show(ex.ToString());
#endif
                                    }
                                }));
                            }
                            break;

                        case DataTableCategory.PlanningSteps:
                            {
                                var viewModel = ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();

                                var columns = viewModel.Schema.Columns.ToArray();

                                List<object[]> rows = xlsManager.Import(columns);

                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    try
                                    {
                                        var defaultColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

                                        foreach (var row in rows)
                                        {
                                            viewModel.NewRow(defaultColumns, row);
                                            var rowId = row[0] as Guid?;
                                            var document = ToBsonDocument(columns, row.ToArray());
                                            AddedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId.Value, document));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex.ToString());
#if DEBUG
                                        MessageBox.Show(ex.ToString());
#endif
                                    }
                                }));
                            }
                            break;

                        case DataTableCategory.ProudctionControl:
                            {
                                var viewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();

                                var columns = viewModel.Schema.Columns.ToArray();

                                List<object[]> rows = xlsManager.Import(columns);

                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                {
                                    try
                                    {
                                        var defaultColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

                                        foreach (var row in rows)
                                        {
                                            viewModel.NewRow(defaultColumns, row);
                                            var rowId = row[0] as Guid?;
                                            var document = ToBsonDocument(columns, row.ToArray());
                                            AddedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId.Value, document));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex.ToString());
#if DEBUG
                                        MessageBox.Show(ex.ToString());
#endif
                                    }
                                }));
                            }
                            break;

                        default:
                            {
                                string logString = "Undefined data table category.";

                                Logger.Error(logString);
#if DEBUG
                                MessageBox.Show(logString);
#endif
                            }
                            break;
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnXlsExportButtonCommand()
        {
            try
            {
                if (!AddedDocuments[_tabControlSelectedIndex].IsEmpty)
                {
                    MessageBox.Show(string.Format("You have added \"{0}\" row(s). Save all changes before to export xls file.", AddedDocuments[_tabControlSelectedIndex].Count));
                    return;
                }

                if (!EditedDocuments[_tabControlSelectedIndex].IsEmpty)
                {
                    MessageBox.Show(string.Format("You have edited \"{0}\" row(s). Save all changes before to export xls file.", EditedDocuments[_tabControlSelectedIndex].Count));
                    return;
                }

                if (!DeletedDocuments[_tabControlSelectedIndex].IsEmpty)
                {
                    MessageBox.Show(string.Format("You have deleted \"{0}\" row(s). Save all changes before to export xls file.", DeletedDocuments[_tabControlSelectedIndex].Count));
                    return;
                }

                var saveFileDialog = new SaveFileDialog
                {
                    Filter = "XLSX documents (.xlsx)|*.xlsx"
                };

                var result = saveFileDialog.ShowDialog();

                if (result.HasValue && !result.Value) return;

                var fileName = saveFileDialog.FileName;

                SleepControlVisibility = true;

                ExecuteTask(() =>
                {
                    try
                    {
                        XlsManager xlsManager = new XlsManager(fileName);

                        var category = (DataTableCategory)_tabControlSelectedIndex;

                        switch (category)
                        {
                            case DataTableCategory.DataStore:
                                {
                                    var repository = new DataStoreRepository(_dataStorage);
                                    var schema = _schemaCache[DefaultSchemas.DataStoreSchemaName];
                                    var documents = repository.FindAll(PrepareFindOptions());
                                    xlsManager.Export(schema.Name, documents, schema.Columns.ToArray());
                                }
                                break;

                            case DataTableCategory.Employees:
                                {
                                    var repository = new DataStoreRepository(_employeesStorage);
                                    var schema = _schemaCache[DefaultSchemas.EmployeesSchemaName];
                                    var documents = repository.FindAll(PrepareFindOptions());
                                    xlsManager.Export(schema.Name, documents, schema.Columns.ToArray());
                                }
                                break;

                            case DataTableCategory.Warehouse:
                                {
                                    var repository = new DataStoreRepository(_warehouseStorage);
                                    var schema = _schemaCache[DefaultSchemas.WarehouseSchemaName];
                                    var documents = repository.FindAll(PrepareFindOptions());
                                    xlsManager.Export(schema.Name, documents, schema.Columns.ToArray());
                                }
                                break;

                            case DataTableCategory.PlanningSteps:
                                {
                                    var repository = new DataStoreRepository(_planningStorage);
                                    var schema = _schemaCache[DefaultSchemas.PlanningStepsSchemaName];
                                    var documents = repository.FindAll(PrepareFindOptions());
                                    xlsManager.Export(schema.Name, documents, schema.Columns.ToArray());
                                }
                                break;

                            case DataTableCategory.ProudctionControl:
                                {
                                    var repository = new DataStoreRepository(_productionStorage);
                                    var schema = _schemaCache[DefaultSchemas.ProductionControlSchemaName];
                                    var documents = repository.FindAll(PrepareFindOptions());
                                    xlsManager.Export(schema.Name, documents, schema.Columns.ToArray());
                                }
                                break;

                            default:
                                {
                                    string logString = "Undefined data table category.";

                                    Logger.Error(logString);
#if DEBUG
                                    MessageBox.Show(logString);
#endif
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.ToString());
#if DEBUG
                        MessageBox.Show(ex.ToString());
#endif
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnPdfExportButtonCommand()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "PDF documents (.pdf)|*.pdf"
            };

            var result = saveFileDialog.ShowDialog();

            if (result.HasValue && !result.Value) return;

            var fileName = saveFileDialog.FileName;
            var viewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();
            var columns = viewModel.Schema.Columns.Where(x => x.Equals(ColumnVisibility.Default)).ToArray();
            var repository = new ProductionControlRepository(_productionStorage);
            var documents = repository.FindAll();

            DocumentBuilder tableBuilder = new DocumentBuilder(fileName);

            var paragraph = new Paragraph("PRODUCTION ORDER SHEET")
            {
                SpacingAfter = 5,
                SpacingBefore = 5           
            };
            
            tableBuilder.AddParagraph(paragraph);

            BaseColor DARK_RED = new BaseColor(170, 50, 50);

            const int FONT_SIZE = 9;

            foreach (var document in documents)
            {
                tableBuilder = tableBuilder.NewTables(4)
                    .AddCell(ProductionControlColumns.PartNumber, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.SapNumber, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.OrderId, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.LotName, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(document[ProductionControlColumns.PartNumber].IsBsonNull ? "" : document[ProductionControlColumns.PartNumber].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.SapNumber].IsBsonNull ? "" : document[ProductionControlColumns.SapNumber].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.OrderId].IsBsonNull ? "" : document[ProductionControlColumns.OrderId].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.LotName].IsBsonNull ? "" : document[ProductionControlColumns.LotName].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .BuildTable();

                tableBuilder = tableBuilder.NewTables(5)
                    .AddCell(ProductionControlColumns.WorkingSteps, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.WireNumber, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.CST, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.Material, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.QTY, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(document[ProductionControlColumns.WorkingSteps].IsBsonNull ? "" : document[ProductionControlColumns.WorkingSteps].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.WireNumber].IsBsonNull ? "" : document[ProductionControlColumns.WireNumber].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.CST].IsBsonNull ? "" : document[ProductionControlColumns.CST].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.Material].IsBsonNull ? "" : document[ProductionControlColumns.Material].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.OrderQty].IsBsonNull ? "" : document[ProductionControlColumns.OrderQty].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .BuildTable();

                tableBuilder = tableBuilder.NewTables(2)
                    .AddCell(ProductionControlColumns.TextProcess, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell("QR", FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(document[ProductionControlColumns.TextProcess].IsBsonNull ? "" : document[ProductionControlColumns.TextProcess].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddQrCode(document["_id"].AsGuid)
                    .BuildTable();

                tableBuilder = tableBuilder.NewTables(5)
                    .AddCell(ProductionControlColumns.MinTimePerLot, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.VisualAid, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.Tool, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.Equipment, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(ProductionControlColumns.LotSize, FONT_SIZE, BaseColor.WHITE, BaseColor.BLACK, 1, true)
                    .AddCell(document[ProductionControlColumns.MinTimePerLot].IsBsonNull ? "" : document[ProductionControlColumns.MinTimePerLot].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.VisualAid].IsBsonNull ? "" : document[ProductionControlColumns.VisualAid].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.Tool].IsBsonNull ? "" : document[ProductionControlColumns.Tool].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.Equipment].IsBsonNull ? "" : document[ProductionControlColumns.Equipment].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .AddCell(document[ProductionControlColumns.LotSize].IsBsonNull ? "" : document[ProductionControlColumns.LotSize].ToString(), FONT_SIZE, BaseColor.LIGHT_GRAY, DARK_RED, 1, false)
                    .BuildTable();

                tableBuilder = tableBuilder.NewTables(1);                  
            }
       
            tableBuilder.Store();
        }

        private void OnSaveChagensButtonCommand()
        {
            try
            {
                var saveChangesBehavior = GetSaveChangesBehavior();

                if (saveChangesBehavior == SaveChangesBehavior.None) return;

                SleepControlVisibility = true;

                var category = (DataTableCategory) _tabControlSelectedIndex;

                ExecuteTask(() =>
                    {
                        try
                        {
                            switch (category)
                            {
                                case DataTableCategory.DataStore:
                                    {
                                        var repository = new DataStoreRepository(_dataStorage);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Going to save \"Date Store\" changes"));

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Save))
                                        {
                                            var addedDocuments = new KeyValuePair<Guid, BsonDocument>[AddedDocuments[_tabControlSelectedIndex].Count];

                                            AddedDocuments[_tabControlSelectedIndex].TryPopRange(addedDocuments);

                                            repository.InsertMany(addedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Edit))
                                        {
                                            var editedDocuments = new KeyValuePair<Guid, BsonDocument>[EditedDocuments[_tabControlSelectedIndex].Count];

                                            EditedDocuments[_tabControlSelectedIndex].TryPopRange(editedDocuments);

                                            repository.UpdateMany(editedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Delete))
                                        {
                                            var deletedDocuments = new KeyValuePair<Guid, BsonDocument>[DeletedDocuments[_tabControlSelectedIndex].Count];

                                            DeletedDocuments[_tabControlSelectedIndex].TryPopRange(deletedDocuments);

                                            repository.DeleteMany(deletedDocuments);

                                        }

                                        Thread.Sleep(500);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: \"Date Store\" changes saved successfully."));
                                    }
                                    break;

                                case DataTableCategory.Employees:
                                    {
                                        var repository = new EmployeesRepository(_employeesStorage);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: Going to save \"Employees\" changes"));

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Save))
                                        {
                                            var addedDocuments = new KeyValuePair<Guid, BsonDocument>[AddedDocuments[_tabControlSelectedIndex].Count];

                                            AddedDocuments[_tabControlSelectedIndex].TryPopRange(addedDocuments);

                                            repository.InsertMany(addedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Edit))
                                        {
                                            var editedDocuments = new KeyValuePair<Guid, BsonDocument>[EditedDocuments[_tabControlSelectedIndex].Count];

                                            EditedDocuments[_tabControlSelectedIndex].TryPopRange(editedDocuments);

                                            repository.UpdateMany(editedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Delete))
                                        {
                                            var deletedDocuments = new KeyValuePair<Guid, BsonDocument>[DeletedDocuments[_tabControlSelectedIndex].Count];

                                            DeletedDocuments[_tabControlSelectedIndex].TryPopRange(deletedDocuments);

                                            repository.DeleteMany(deletedDocuments);

                                        }

                                        Thread.Sleep(500);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: \"Employees\" changes saved successfully."));
                                    }
                                    break;

                                case DataTableCategory.Warehouse:
                                    {
                                        var repository = new WarehouseRepository(_warehouseStorage);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: Going to save \"Warehouse\" changes"));

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Save))
                                        {
                                            var addedDocuments = new KeyValuePair<Guid, BsonDocument>[AddedDocuments[_tabControlSelectedIndex].Count];

                                            AddedDocuments[_tabControlSelectedIndex].TryPopRange(addedDocuments);

                                            repository.InsertMany(addedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Edit))
                                        {
                                            var editedDocuments = new KeyValuePair<Guid, BsonDocument>[EditedDocuments[_tabControlSelectedIndex].Count];

                                            EditedDocuments[_tabControlSelectedIndex].TryPopRange(editedDocuments);

                                            repository.UpdateMany(editedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Delete))
                                        {
                                            var deletedDocuments = new KeyValuePair<Guid, BsonDocument>[DeletedDocuments[_tabControlSelectedIndex].Count];

                                            DeletedDocuments[_tabControlSelectedIndex].TryPopRange(deletedDocuments);

                                            repository.DeleteMany(deletedDocuments);

                                        }

                                        Thread.Sleep(500);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: \"Employees\" changes saved successfully."));
                                    }
                                    break;

                                case DataTableCategory.PlanningSteps:
                                    {
                                        var repository = new PlanningStepsRepository(_planningStorage);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: Going to save \"Planning Steps\" changes"));

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Save))
                                        {
                                            var addedDocuments = new KeyValuePair<Guid, BsonDocument>[AddedDocuments[_tabControlSelectedIndex].Count];

                                            AddedDocuments[_tabControlSelectedIndex].TryPopRange(addedDocuments);

                                            repository.InsertMany(addedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Edit))
                                        {
                                            var editedDocuments = new KeyValuePair<Guid, BsonDocument>[EditedDocuments[_tabControlSelectedIndex].Count];

                                            EditedDocuments[_tabControlSelectedIndex].TryPopRange(editedDocuments);

                                            repository.UpdateMany(editedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Delete))
                                        {
                                            var deletedDocuments = new KeyValuePair<Guid, BsonDocument>[DeletedDocuments[_tabControlSelectedIndex].Count];

                                            DeletedDocuments[_tabControlSelectedIndex].TryPopRange(deletedDocuments);

                                            repository.DeleteMany(deletedDocuments);
                                        }

                                        Thread.Sleep(500);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: \"Planning Steps\" changes saved successfully."));
                                    }
                                    break;

                                case DataTableCategory.ProudctionControl:
                                    {
                                        var repository = new ProductionControlRepository(_productionStorage);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: Going to save \"Production Control\" changes"));

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Save))
                                        {
                                            var addedDocuments = new KeyValuePair<Guid, BsonDocument>[AddedDocuments[_tabControlSelectedIndex].Count];

                                            AddedDocuments[_tabControlSelectedIndex].TryPopRange(addedDocuments);

                                            repository.InsertMany(addedDocuments.Select(x => x.Value));
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Edit))
                                        {
                                            var editedDocuments = new KeyValuePair<Guid, BsonDocument>[EditedDocuments[_tabControlSelectedIndex].Count];

                                            EditedDocuments[_tabControlSelectedIndex].TryPopRange(editedDocuments);

                                            repository.UpdateMany(editedDocuments);
                                        }

                                        if (saveChangesBehavior.HasFlag(SaveChangesBehavior.Delete))
                                        {
                                            var deletedDocuments = new KeyValuePair<Guid, BsonDocument>[DeletedDocuments[_tabControlSelectedIndex].Count];

                                            DeletedDocuments[_tabControlSelectedIndex].TryPopRange(deletedDocuments);

                                            repository.DeleteMany(deletedDocuments);
                                        }

                                        Thread.Sleep(500);

                                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LastLoggedAction = "Last Action: \"Production Control\" changes saved successfully."));
                                    }
                                    break;

                                default:
                                    {
                                        throw new ArgumentOutOfRangeException($"_tabControlSelectedIndex");
                                    }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex.ToString());
#if DEBUG
                            MessageBox.Show(ex.ToString());
#endif
                        }
                    });
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnOpenSettingsButtonCommand()
        {
            try
            {
                var settingsWindow = new SettingsWindow
                    {
                        Title = "Coroplast - Settings",
                        Width = 600,
                        Height = 400,
                        ResizeMode = ResizeMode.NoResize,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen
                    };

                bool? result = settingsWindow.ShowDialog();

                if (!result.HasValue)
                {
                    throw new Exception("Settings window fail");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnDataGridLoadingRowCommand(DataGridRowEventArgs args)
        {
            //            var category = (DataTableCategory) _tabControlSelectedIndex;
            //
            //            if (category == DataTableCategory.PlanningSteps)
            //            {
            //                args.Row.Background = new SolidColorBrush(Colors.DarkSalmon);
            //                args.Row.Foreground = new SolidColorBrush(Colors.DarkSalmon);
            //                args.Row.BorderBrush = new SolidColorBrush(Colors.DarkSalmon);
            //            }
        }

        private void OnDataGridDoubleClickCommand(MouseButtonEventArgs args)
        {
            //var category = (DataTableCategory) _tabControlSelectedIndex;

            //switch (category)
            //{
            //    case DataTableCategory.DataStore:
            //        break;
            //    case DataTableCategory.Employees:
            //        break;
            //    case DataTableCategory.Warehouse:
            //        break;
            //    case DataTableCategory.PlanningSteps:
            //        break;
            //    case DataTableCategory.ProudctionControl:
            //        break;
            //    default:
            //        throw new ArgumentOutOfRangeException();
            //}
        }

        private void OnTabControlSelectedIndexCommand(DataTableCategory category)
        {
            try
            {
#if DEBUG
                if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
#endif

                CleanUpQueryExpression();

                ShowAdditionalButtons(category);

                switch (category)
                {
                    case DataTableCategory.DataStore:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<DataStoreViewModel>();

                            var columns = viewModel.Schema.Columns;

                            FilterQueryTokenizers(columns);

                            LastLoggedAction = "";

                            ExecuteTask(() =>
                            {
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = true));
                                Thread.Sleep(DEFAULT_DELAY_TIME);
                                if (viewModel.DataTable.Rows.Count > 0) return;
                                AddNewRowSet(LoadDataStore(columns), columns, viewModel);
                            });
                        }
                        break;

                    case DataTableCategory.Employees:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<EmployeesViewModel>();

                            var columns = viewModel.Schema.Columns;

                            FilterQueryTokenizers(columns);

                            ExecuteTask(() =>
                            {
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = true));
                                Thread.Sleep(DEFAULT_DELAY_TIME);                          
                                if (viewModel.DataTable.Rows.Count > 0) return;
                                AddNewRowSet(LoadEmployees(columns), columns, viewModel);
                            });
                        }
                        break;

                    case DataTableCategory.Warehouse:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();

                            var columns = viewModel.Schema.Columns;

                            FilterQueryTokenizers(columns);

                            ExecuteTask(() =>
                            {
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = true));
                                Thread.Sleep(DEFAULT_DELAY_TIME);
                                if (viewModel.DataTable.Rows.Count > 0) return;
                                AddNewRowSet(LoadWarehouse(columns), columns, viewModel);
                            });
                        }
                        break;

                    case DataTableCategory.PlanningSteps:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();

                            var columns = viewModel.Schema.Columns;

                            FilterQueryTokenizers(columns);

                            ExecuteTask(() =>
                            {
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = true));
                                Thread.Sleep(DEFAULT_DELAY_TIME);
                                if (viewModel.DataTable.Rows.Count > 0) return;
                                AddNewRowSet(LoadPlanningSteps(columns), columns, viewModel);
                            });
                        }
                        break;

                    case DataTableCategory.ProudctionControl:
                        {
                            var viewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();

                            var columns = viewModel.Schema.Columns;

                            FilterQueryTokenizers(columns);

                            viewModel.DataTable.Clear();

                            ExecuteTask(() =>
                                {
                                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = true));
                                    Thread.Sleep(DEFAULT_DELAY_TIME);
                                    //todo: I have to calculate just once and not each time
                                    ComputeProductionControl();
                                    AddNewRowSet(LoadProductionControl(columns), columns, viewModel);
                                });
                        }
                        break;

                    default:
                        {
                            var logString = "Tab control invalid state";

                            Logger.Error(logString);
#if DEBUG
                            MessageBox.Show(logString);
#endif
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void OnDataGridSelectionChangedCommand(SelectionChangedEventArgs args)
        {
            //            MessageBox.Show("DATASTORE SELECTION CHANGED");
        }

        private void OnQueryStringTokenizedTextBoxCommand(KeyEventArgs e)
        {
        }

        private void ExecuteTask(Action callback)
        {
            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        callback();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.ToString());
#if DEBUG
                        MessageBox.Show(ex.ToString());
#endif
                    }
                }).ContinueWith(r =>
                {
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = false));
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.ToString());
#if DEBUG
                        MessageBox.Show(ex.ToString());
#endif
                    }
                });
        }

        private void InsertChangeLogAsync(Guid rowId, object[] row)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    var changeLog = new List<object>
                            {
                                rowId,
                                DateTime.Now
                            };

                    changeLog.AddRange(row);

                    var changeLogViewModel = ServiceLocator.Current.GetInstance<ChangeLogViewModel>();

                    var changeLogRepository = new ChangeLogRepository(_changeLogStorage);

                    changeLogRepository.InsertOne(ToBsonDocument(changeLogViewModel.Schema.Columns.ToArray(), changeLog));
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.ToString());
#if DEBUG
                    MessageBox.Show(ex.ToString());
#endif
                }
            });
        }

        private void CleanUpQueryExpression()
        {
            QueryString = string.Empty;
            OnQueryButtonCommand();
        }

        private void ShowAdditionalButtons(DataTableCategory category)
        {
            switch (category)
            {
                case DataTableCategory.Employees:
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => ScanningVisibility = true));
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => ChangeLogVisibility = false));
                    }
                    break;

                case DataTableCategory.Warehouse:
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => ScanningVisibility = false));
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => ChangeLogVisibility = true));
                    }
                    break;

                default:
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => ScanningVisibility = ChangeLogVisibility = false));               
                    }
                    break;
            }
        }

        private void AddNewVirtualRow(object[] row, IList<IColumnData> columns)
        {
            var rowId = row[0] as Guid?;

            if (!rowId.HasValue) throw new Exception("For some reason could not cast row id as Guid type.");

            var document = ToBsonDocument(columns.ToArray(), row);

            AddedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId.Value, document));
        }

        private void NewEditedVirtualRow(DataRow row, IColumnData[] filteredColumns, object[] modifiedRow)
        {
            var rowId = (Guid) row.GetTag();

            var document = ToBsonDocument(filteredColumns, modifiedRow);

            EditedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId, document));
        }

        private void NewDeletedVirtualRow(DataRow row)
        {
            var rowId = row.GetTag() as Guid?;

            if (!rowId.HasValue) throw new Exception("For some reason could not cast row id as Guid type.");

            DeletedDocuments[_tabControlSelectedIndex].Push(new KeyValuePair<Guid, BsonDocument>(rowId.Value, null));
        }

        private void FilterQueryTokenizers(IEnumerable<IColumnData> columns)
        {
            QueryTokenizers = new ObservableCollection<string>(columns.Select(x => x.Alias).Union(_tokenizers));
        }

        private void InitializeSchemaCaches()
        {
            //-----

            var dataStoreViewModel = ServiceLocator.Current.GetInstance<DataStoreViewModel>();

            if (!_schemaCache.ContainsKey(dataStoreViewModel.Schema.Name))
            {
                _schemaCache.Add(dataStoreViewModel.Schema.Name, dataStoreViewModel.Schema);
            }

            //-----

            var employeesViewModel = ServiceLocator.Current.GetInstance<EmployeesViewModel>();

            if (!_schemaCache.ContainsKey(employeesViewModel.Schema.Name))
            {
                _schemaCache.Add(employeesViewModel.Schema.Name, employeesViewModel.Schema);
            }

            //-----

            var warehouseViewModel = ServiceLocator.Current.GetInstance<WarehouseViewModel>();

            if (!_schemaCache.ContainsKey(warehouseViewModel.Schema.Name))
            {
                _schemaCache.Add(warehouseViewModel.Schema.Name, warehouseViewModel.Schema);
            }

            //-----

            var planningStepsViewModel = ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();

            if (!_schemaCache.ContainsKey(planningStepsViewModel.Schema.Name))
            {
                _schemaCache.Add(planningStepsViewModel.Schema.Name, planningStepsViewModel.Schema);
            }

            //-----

            var productionControlViewModel = ServiceLocator.Current.GetInstance<ProductionControlViewModel>();

            if (!_schemaCache.ContainsKey(productionControlViewModel.Schema.Name))
            {
                _schemaCache.Add(productionControlViewModel.Schema.Name, productionControlViewModel.Schema);
            }

            //-----

            var scanningViewModel = ServiceLocator.Current.GetInstance<ScanningViewModel>();

            if (!_schemaCache.ContainsKey(scanningViewModel.Schema.Name))
            {
                _schemaCache.Add(scanningViewModel.Schema.Name, scanningViewModel.Schema);
            }

            //-----
        }

        private void ComputeProductionControl()
        {
            var columns = ServiceLocator.Current.GetInstance<ProductionControlViewModel>().Schema.Columns;

            var dataStoreRepository = new DataStoreRepository(_dataStorage);

            var planningStepsRepository = new PlanningStepsRepository(_planningStorage);

            var productionControlRepository = new ProductionControlRepository(_productionStorage);

            var sort = new BsonDocumentSortDefinition<BsonDocument>(new BsonDocument
                {
                        {"_AT", 1}
                });

            //todo: There is a workaround inside method Lookup for SapNumber column.
            var documents = dataStoreRepository.Lookup(StorageCollections.PlanningDataCollectonName, "PartNumber", "PartNumber", "Plannings", sort);

            var productions = new List<BsonDocument>();

            foreach (var document in documents)
            {
                var plannings = document["Plannings"].AsBsonArray;

                var lotId = productionControlRepository.FindMaxLotName();

                foreach (var planning in plannings)
                {
                    var orderQty = planning[PlanningStepsColumns.OrderQty].AsInt32;

                    int lastStep;

                    //steps count = orderQty / 100
                    //last step = orderQty % 100             
                    var stepsCount = Math.DivRem(orderQty, 100, out lastStep);

                    for (var i = 0; i < stepsCount; i++)
                    {
                        var row = new List<object>
                            {
                                Guid.NewGuid(),
                                DateTime.Now,
                                document[DataStoreColumns.PartNumber].IsBsonNull ? null : document[DataStoreColumns.PartNumber].AsString,
                                document[DataStoreColumns.SapNumber].AsNullableInt32,
                                document[DataStoreColumns.WorkingSteps].AsNullableInt32,
                                document[DataStoreColumns.WireNumber].AsNullableInt32,
                                document[DataStoreColumns.Cst].AsNullableInt32,
                                document[DataStoreColumns.Material].IsBsonNull ? null : document[DataStoreColumns.Material].AsString,
                                document[DataStoreColumns.Unit].AsNullableInt32,
                                document[DataStoreColumns.Qty].AsNullableInt32,
                                document[DataStoreColumns.TextProcess].IsBsonNull ? null : document[DataStoreColumns.TextProcess].AsString,
                                document[DataStoreColumns.MinPer100].AsNullableInt32,
                                document[DataStoreColumns.Equipment].AsNullableInt32,
                                document[DataStoreColumns.Strip1].IsBsonNull ? null : document[DataStoreColumns.Strip1].AsString,
                                document[DataStoreColumns.Strip2].IsBsonNull ? null : document[DataStoreColumns.Strip2].AsString,
                                document[DataStoreColumns.Tool].AsNullableInt32,
                                document[DataStoreColumns.Side].AsNullableInt32,
                                document[DataStoreColumns.VisualAid].IsBsonNull ? null : document[DataStoreColumns.VisualAid].AsString,
                                document[DataStoreColumns.Cav].AsNullableInt32,
                                document[DataStoreColumns.Wires1].AsNullableInt32,
                                document[DataStoreColumns.Wires2].AsNullableInt32,
                                document[DataStoreColumns.Wires3].AsNullableInt32,
                                planning[PlanningStepsColumns.OrderId].AsNullableInt64,
                                planning[PlanningStepsColumns.OrderQty].AsNullableInt32,
                                i + 1, //OrderStep
                                planning[PlanningStepsColumns.Group].IsBsonNull ? null : planning[PlanningStepsColumns.Group].AsString,
                                100,
                                $"L{lotId}",
                                document[DataStoreColumns.MinPer100].IsBsonNull ? (object) null : document[DataStoreColumns.MinPer100].AsInt32 / 100d * 100d,
                                null, //OperatorName
                                null, //OperatorNumber                             
                                null, //Shift
                                null, //TeamGroup
                                null, //Date
                            };

                        productions.Add(ToBsonDocument(columns.ToArray(), row));
                    }

                    if (lastStep > 0)
                    {
                        var row = new List<object>
                            {
                                Guid.NewGuid(),
                                DateTime.Now,
                                document[DataStoreColumns.PartNumber].IsBsonNull ? null : document[DataStoreColumns.PartNumber].AsString,
                                document[DataStoreColumns.SapNumber].AsNullableInt32,
                                document[DataStoreColumns.WorkingSteps].AsNullableInt32,
                                document[DataStoreColumns.WireNumber].AsNullableInt32,
                                document[DataStoreColumns.Cst].AsNullableInt32,
                                document[DataStoreColumns.Material].IsBsonNull ? null : document[DataStoreColumns.Material].AsString,
                                document[DataStoreColumns.Unit].AsNullableInt32,
                                document[DataStoreColumns.Qty].AsNullableInt32,
                                document[DataStoreColumns.TextProcess].IsBsonNull ? null : document[DataStoreColumns.TextProcess].AsString,
                                document[DataStoreColumns.MinPer100].AsNullableInt32,
                                document[DataStoreColumns.Equipment].AsNullableInt32,
                                document[DataStoreColumns.Strip1].IsBsonNull ? null : document[DataStoreColumns.Strip1].AsString,
                                document[DataStoreColumns.Strip2].IsBsonNull ? null : document[DataStoreColumns.Strip2].AsString,
                                document[DataStoreColumns.Tool].AsNullableInt32,
                                document[DataStoreColumns.Side].AsNullableInt32,
                                document[DataStoreColumns.VisualAid].IsBsonNull ? null : document[DataStoreColumns.VisualAid].AsString,
                                document[DataStoreColumns.Cav].AsNullableInt32,
                                document[DataStoreColumns.Wires1].AsNullableInt32,
                                document[DataStoreColumns.Wires2].AsNullableInt32,
                                document[DataStoreColumns.Wires3].AsNullableInt32,
                                planning[PlanningStepsColumns.OrderId].AsNullableInt64,
                                planning[PlanningStepsColumns.OrderQty].AsNullableInt32,
                                stepsCount + 1, //OrderStep
                                planning[PlanningStepsColumns.Group].IsBsonNull ? null : planning[PlanningStepsColumns.Group].AsString,
                                lastStep,
                                $"L{lotId}",
                                document[DataStoreColumns.MinPer100].IsBsonNull ? (object) null : document[DataStoreColumns.MinPer100].AsInt32 / 100d * lastStep,
                                null, //OperatorName
                                null, //OperatorNumber                             
                                null, //Shift
                                null, //TeamGroup
                                null, //Date
                            };

                        productions.Add(ToBsonDocument(columns.ToArray(), row));
                    }

                    planningStepsRepository.UpdateState(planning["_id"].AsGuid);
                }
            }

            if (productions.Count > 0)
            {
                productionControlRepository.InsertMany(productions);
            }
        }

        private List<object>[] LoadDataStore(IEnumerable<IColumnData> columns)
        {
            var repository = new DataStoreRepository(_dataStorage);
            return GetRows(columns.ToList(), repository.FindAll(PrepareFindOptions()));
        }

        private List<object>[] LoadEmployees(IEnumerable<IColumnData> columns)
        {
            var repository = new EmployeesRepository(_employeesStorage);
            return GetRows(columns.ToList(), repository.FindAll(PrepareFindOptions()));
        }

        private List<object>[] LoadWarehouse(IEnumerable<IColumnData> columns)
        {
            var repository = new WarehouseRepository(_warehouseStorage);
            return GetRows(columns.ToList(), repository.FindAll(PrepareFindOptions()));
        }

        private List<object>[] LoadPlanningSteps(IEnumerable<IColumnData> columns)
        {
            var repository = new PlanningStepsRepository(_planningStorage);
            return GetRows(columns.ToList(), repository.FindAll(PrepareFindOptions()));
        }

        private List<object>[] LoadProductionControl(IEnumerable<IColumnData> columns)
        {
            var repository = new ProductionControlRepository(_productionStorage);
            return GetRows(columns.ToList(), repository.FindAll(PrepareFindOptions()));
        }

        private SaveChangesBehavior GetSaveChangesBehavior()
        {
            var saveChangesBehavior = SaveChangesBehavior.None;

            if (!AddedDocuments[_tabControlSelectedIndex].IsEmpty)
            {
                saveChangesBehavior |= SaveChangesBehavior.Save;
            }

            if (!EditedDocuments[_tabControlSelectedIndex].IsEmpty)
            {
                saveChangesBehavior |= SaveChangesBehavior.Edit;
            }

            if (!DeletedDocuments[_tabControlSelectedIndex].IsEmpty)
            {
                saveChangesBehavior |= SaveChangesBehavior.Delete;
            }

            return saveChangesBehavior;
        }

        //-----

        private static void TestConnection()
        {
            if (!StorageManager.TestConnection(DefaultConnectionSettings.GetConnectionSettings(), ex => Logger.Error(ex.ToString())))
            {
                throw new Exception("A network-related or instance-specific error occurred while establishing a connection to database server. The server was not found or was not accessible.");
            }
        }

        private static void AddNewRowSet(List<object>[] rows, IList<IColumnData> columns, DataStoreViewModel viewModel)
        {
            if (rows.Length <= 0) return;

            var filteredColumns = columns.Where(x => x.Equals(ColumnVisibility.Default));

            foreach (var row in rows)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        //todo: Add try catch block or better wrap dispatcher into method
                        viewModel.NewRow(filteredColumns, row.ToArray());
                    }));
            }
        }

        private static void AddNewRowSet(List<object>[] rows, IList<IColumnData> columns, EmployeesViewModel viewModel)
        {
            if (rows.Length <= 0) return;

            var filteredColumns = columns.Where(x => x.Equals(ColumnVisibility.Default));

            foreach (var row in rows)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        //todo: Add try catch block or better wrap dispatcher into method
                        viewModel.NewRow(filteredColumns, row.ToArray());
                    }));
            }
        }

        private static void AddNewRowSet(List<object>[] rows, IList<IColumnData> columns, WarehouseViewModel viewModel)
        {
            if (rows.Length <= 0) return;

            var filteredColumns = columns.Where(x => x.Equals(ColumnVisibility.Default));

            foreach (var row in rows)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        //todo: Add try catch block or better wrap dispatcher into method
                        viewModel.NewRow(filteredColumns, row.ToArray());
                    }));
            }
        }

        private static void AddNewRowSet(List<object>[] rows, IList<IColumnData> columns, PlanningStepsViewModel viewModel)
        {
            if (rows.Length <= 0) return;

            var filteredColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

            foreach (var row in rows)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => viewModel.NewRow(filteredColumns, row.ToArray())));
            }
        }

        private static void AddNewRowSet(List<object>[] rows, IList<IColumnData> columns, ProductionControlViewModel viewModel)
        {
            if (rows.Length <= 0) return;

            var filteredColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

            foreach (var row in rows)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => viewModel.NewRow(filteredColumns, row.ToArray())));
            }
        }

        private static BsonDocument ToBsonDocument(IColumnData[] columns, IList<object> row)
        {
            var length = columns.Length;

            var document = new BsonDocument();

            for (var index = 0; index < length; index++)
            {
                var type = Type.GetType(columns[index].Type);

                if (type == null)
                {
                    throw new TypeLoadException(string.Format("Field type \"{0}\" is not well formed.", columns[index].Type));
                }

                var typeCode = Type.GetTypeCode(type);

                switch (typeCode)
                {
                    case TypeCode.Empty:
                        {
                            document.Add(columns[index].Name, BsonValue.Create(""));
                        }
                        break;
                    case TypeCode.Object:
                        {
                            if (type.IsAssignableFrom(typeof(Guid)))
                            {
                                var item = row[index];

                                if (item == null)
                                {
                                    document.Add(columns[index].Name, BsonNull.Value);
                                }
                                else
                                {
                                    Guid value;

                                    document.Add(columns[index].Name, !Guid.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                                }
                            }
                        }
                        break;
                    case TypeCode.DBNull:
                        {
                        }
                        break;
                    case TypeCode.Boolean:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                bool value;

                                document.Add(columns[index].Name, !bool.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Char:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                int value;

                                document.Add(columns[index].Name, !int.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.SByte:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                int value;

                                document.Add(columns[index].Name, !int.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Byte:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                int value;

                                document.Add(columns[index].Name, !int.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Int16:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                int value;

                                document.Add(columns[index].Name, !int.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.UInt16:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                int value;

                                document.Add(columns[index].Name, !int.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Int32:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                int value;

                                document.Add(columns[index].Name, !int.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.UInt32:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                int value;

                                document.Add(columns[index].Name, !int.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Int64:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                long value;

                                document.Add(columns[index].Name, !long.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.UInt64:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                long value;

                                document.Add(columns[index].Name, !long.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Single:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                double value;

                                document.Add(columns[index].Name, !double.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Double:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                double value;

                                document.Add(columns[index].Name, !double.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.Decimal:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                decimal value;

                                document.Add(columns[index].Name, !decimal.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value));
                            }
                        }
                        break;
                    case TypeCode.DateTime:
                        {
                            var item = row[index];

                            if (item == null)
                            {
                                document.Add(columns[index].Name, BsonNull.Value);
                            }
                            else
                            {
                                DateTime value;

                                var obj = !DateTime.TryParse(item.ToString(), out value) ? BsonNull.Value : BsonValue.Create(value);

                                document.Add(columns[index].Name, obj);
                            }
                        }
                        break;
                    case TypeCode.String:
                        {
                            var value = row[index];

                            document.Add(columns[index].Name, value == null ? BsonNull.Value : BsonValue.Create(value.ToString()));
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return document;
        }

        private static List<object>[] GetRows(IList<IColumnData> columns, IEnumerable<BsonDocument> documents)
        {
            var rows = new List<List<object>>();

            foreach (var document in documents)
            {
                var row = new List<object>();

                foreach (var column in columns)
                {
                    var type = Type.GetType(column.Type);

                    if (type == null)
                    {
                        throw new TypeLoadException($"Field type \"{column.Type}\" is not well formed.");
                    }

                    var typeCode = Type.GetTypeCode(type);

                    switch (typeCode)
                    {
                        case TypeCode.Empty:
                            {
                                row.Add(null);
                            }
                            break;
                        case TypeCode.Object:
                            {
                                if (type.IsAssignableFrom(typeof(Guid)))
                                {
                                    row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableGuid);
                                }
                            }
                            break;
                        case TypeCode.DBNull:
                            {
                                row.Add(null);
                            }
                            break;
                        case TypeCode.Boolean:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableBoolean);
                            }
                            break;
                        case TypeCode.Char:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.SByte:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Byte:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Int16:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.UInt16:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Int32:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.UInt32:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt32);
                            }
                            break;
                        case TypeCode.Int64:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt64);
                            }
                            break;
                        case TypeCode.UInt64:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableInt64);
                            }
                            break;
                        case TypeCode.Single:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableDouble);
                            }
                            break;
                        case TypeCode.Double:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableDouble);
                            }
                            break;
                        case TypeCode.Decimal:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsNullableDecimal);
                            }
                            break;
                        case TypeCode.DateTime:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].ToNullableLocalTime());
                            }
                            break;
                        case TypeCode.String:
                            {
                                row.Add(document[column.Name].IsBsonNull ? null : document[column.Name].AsString);
                            }
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                rows.Add(row);
            }

            return rows.ToArray();
        }

        private static AddRowViewModel ShowAddRowDialogWindow(IEnumerable<IColumnData> columns)
        {
            var addRowViewModel = new AddRowViewModel(columns);

            var window = new AddRowWindow
                {
                    DataContext = addRowViewModel
                };

            window.ShowDialog();

            return addRowViewModel;
        }

        private static EditRowViewModel OpenEditRowDialogWindow(IList<IColumnData> columns, object[] row)
        {
            var editRowViewModel = new EditRowViewModel(columns, row);

            var window = new EditRowWindow
                {
                    DataContext = editRowViewModel
                };

            window.ShowDialog();

            return editRowViewModel;
        }

        private static FindOptions<BsonDocument> PrepareFindOptions()
        {
            var findOptions = new FindOptions<BsonDocument>
                {
                    Sort = new BsonDocumentSortDefinition<BsonDocument>(new BsonDocument
                        {
                                {"AT", 1}
                        })
                };

            return findOptions;
        }

        //-----

        #region XLS MAP

        private readonly string[] _xlsMap = {
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z",
                "AA",
                "AB",
                "AC",
                "AD",
                "AE",
                "AF",
                "AG",
                "AH",
                "AI",
                "AJ",
                "AK",
                "AL",
                "AM",
                "AN",
                "AO",
                "AP",
                "AQ",
                "AR",
                "AS",
                "AT",
                "AU",
                "AV",
                "AW",
                "AX",
                "AY",
            };

        #endregion

        //-----
    }
}