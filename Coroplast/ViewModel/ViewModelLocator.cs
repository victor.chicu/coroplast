/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Coroplast"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace Coroplast.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.AsDefault.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.AsDefault.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<SignInViewModel>();
            SimpleIoc.Default.Register<AddRowViewModel>();
            SimpleIoc.Default.Register<SettingsViewModel>();
            SimpleIoc.Default.Register<ScanningViewModel>();
            SimpleIoc.Default.Register<ChangeLogViewModel>();
            SimpleIoc.Default.Register<DataTableViewModel>();
            SimpleIoc.Default.Register<DataStoreViewModel>();
            SimpleIoc.Default.Register<EmployeesViewModel>();
            SimpleIoc.Default.Register<WarehouseViewModel>();
            SimpleIoc.Default.Register<PlanningStepsViewModel>();
            SimpleIoc.Default.Register<ProductionControlViewModel>();
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public SignInViewModel SignIn => ServiceLocator.Current.GetInstance<SignInViewModel>();
        public AddRowViewModel AddNewRow => ServiceLocator.Current.GetInstance<AddRowViewModel>();
        public SettingsViewModel Settings => ServiceLocator.Current.GetInstance<SettingsViewModel>();
        public ScanningViewModel Scanning => ServiceLocator.Current.GetInstance<ScanningViewModel>();
        public ChangeLogViewModel ChangeLog => ServiceLocator.Current.GetInstance<ChangeLogViewModel>();
        public DataTableViewModel DataTable => ServiceLocator.Current.GetInstance<DataTableViewModel>();
        public DataStoreViewModel DataStore => ServiceLocator.Current.GetInstance<DataStoreViewModel>();
        public EmployeesViewModel Employees => ServiceLocator.Current.GetInstance<EmployeesViewModel>();
        public WarehouseViewModel Warehouse => ServiceLocator.Current.GetInstance<WarehouseViewModel>();
        public PlanningStepsViewModel PlanningSteps => ServiceLocator.Current.GetInstance<PlanningStepsViewModel>();
        public ProductionControlViewModel ProductionControl => ServiceLocator.Current.GetInstance<ProductionControlViewModel>();

        public static void Cleanup()
        {
        }
    }
}