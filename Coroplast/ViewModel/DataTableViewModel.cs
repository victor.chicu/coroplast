﻿using System.Data;
using GalaSoft.MvvmLight;

namespace Coroplast.ViewModel
{
    public class DataTableViewModel : ViewModelBase
    {
        //-----

        private object _selectedItem;

        private DataTable _dataTable;

        //-----

        public DataTableViewModel()
        {
            _dataTable = new DataTable();
        }

        //-----

        public object SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                Set(() => SelectedItem, ref _selectedItem, value);
            }
        }

        public DataTable DataTable
        {
            get { return _dataTable; }
            set
            {
                Set(() => DataTable, ref _dataTable, value);
            }
        }

        //-----
    }
}
