﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using Coroplast.Model;
using Coroplast.Objects;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Coroplast.ViewModel
{
    public class EditRowViewModel : ViewModelBase
    {
        //-----
 
        private RelayCommand<Window> _updateCommand;

        private ObservableCollection<Column> _columns;

        private readonly List<object> _modifiedRow = new List<object>();

        private readonly Dictionary<string, bool> _validationErrors;

        //-----

        public EditRowViewModel()
        {
            _columns = new ObservableCollection<Column>();
            _updateCommand = new RelayCommand<Window>(OnUpdateCommand, TryUpdateCommand);
            _validationErrors = new Dictionary<string, bool>();
        }

        public EditRowViewModel(IList<IColumnData> columns, object[] row) : this()
        {
#if DEBUG
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;

#endif

            for (var index = 0; index < row.Length; index++)
            {
                Columns.Add(CreateColumn(columns[index], row[index], true));
            }
        }

        //-----

        public RelayCommand<Window> UpdateCommand
        {
            get { return _updateCommand; }
            set { Set(() => UpdateCommand, ref _updateCommand, value); }
        }

        public ObservableCollection<Column> Columns
        {
            get { return _columns; }
            set { Set(() => Columns, ref _columns, value); }
        }

        //-----

        private void OnUpdateCommand(Window window)
        {
            foreach (var column in Columns)
            {
                var type = Type.GetType(column.Type);

                if (type == null)
                {
                    throw new TypeLoadException($"Field type \"{column.Type}\" is not well formed.");
                }

                var typeCode = Type.GetTypeCode(type);

                switch (typeCode)
                {
                    case TypeCode.Empty:
                        {
                            _modifiedRow.Add(string.Empty);
                        }
                        break;
                    case TypeCode.Object:
                        if (type.IsAssignableFrom(typeof(Guid)))
                        {
                            _modifiedRow.Add(column.Randomly ? Guid.NewGuid() : column.DefaultValue);
                        }
                        else
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.DBNull:
                        {
                            _modifiedRow.Add(DBNull.Value);
                        }
                        break;
                    case TypeCode.Boolean:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Char:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.SByte:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Byte:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Int16:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.UInt16:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Int32:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.UInt32:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Int64:
                        {
                            _modifiedRow.Add(column.DefaultValue);

                        }
                        break;
                    case TypeCode.UInt64:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Single:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Double:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.Decimal:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    case TypeCode.DateTime:
                        {
                            if (column.DefaultValue != null)
                            {
                                try
                                {
                                    if (column.DefaultValue is DateTime)
                                    {
                                        _modifiedRow.Add(column.DefaultValue);
                                    }
                                    else
                                    {
                                        var formats = new[]
                                            {
                                                "dd/MM/yyyy",
                                                "dd.MM.yyyy",
                                            };

                                        var provider = CultureInfo.InvariantCulture;

                                        try
                                        {
                                            _modifiedRow.Add(DateTime.ParseExact(column.DefaultValue.ToString(), formats, provider, DateTimeStyles.AssumeLocal));
                                        }
                                        catch (FormatException)
                                        {
                                            _modifiedRow.Add(DBNull.Value);
                                        }
                                    }
                                }
                                catch (FormatException)
                                {
                                    _modifiedRow.Add(DBNull.Value);
                                }
                            }
                            else
                            {
                                _modifiedRow.Add(DBNull.Value);
                            }
                        }
                        break;
                    case TypeCode.String:
                        {
                            _modifiedRow.Add(column.DefaultValue);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            window?.Close();
        }

        private void CellPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var cell = (Column)sender;

            var type = Type.GetType(cell.Type);

            if (type == null) throw new TypeLoadException(cell.Type);

            if (type.IsAssignableFrom(typeof(int)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    int value;

                    if (!int.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is an integer type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(long)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    long value;

                    if (!long.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a long type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(Guid)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    Guid value;

                    if (!Guid.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is an id type");
                    }

                    if (value.Equals(Guid.Empty))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be empty id");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(float)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    float value;

                    if (!float.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a float type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(double)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    double value;

                    if (!double.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a double type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(decimal)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (!isEmpty)
                {
                    decimal value;

                    if (!decimal.TryParse(cell.DefaultValue as string, out value))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a decimal type");
                    }

                    if (value < 0)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("The default value can not be less than zero");
                    }
                }
                else
                {
                    cell.DefaultValue = null;
                }
            }
            else if (type.IsAssignableFrom(typeof(DateTime)))
            {
                if (cell.DefaultValue == null)
                {
                    return;
                }

                var isEmpty = ReferenceEquals(cell.DefaultValue, string.Empty);

                if (isEmpty)
                {
                    cell.DefaultValue = null;
                }
                else
                {
                    var defaultValue = cell.DefaultValue.ToString();

                    if (defaultValue.StartsWith("-"))
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a date time type");
                    }

                    var formats = new[]
                        {
                            "dd/MM/yyyy",
                            "dd.MM.yyyy",
                        };
                
                    var provider = CultureInfo.InvariantCulture;

                    try
                    {
                        DateTime.ParseExact(defaultValue, formats, provider, DateTimeStyles.AssumeLocal);
                    }
                    catch (FormatException)
                    {
                        _validationErrors[cell.Title] = true;
                        throw new Exception("This column is a date time type");
                    }
                }
            }

            _validationErrors[cell.Title] = false;
        }

        private bool TryUpdateCommand(Window window)
        {
            return !_validationErrors.Any(x => x.Value);
        }

        private Column CreateColumn(IColumnData column, object @object, bool withEvent = false)
        {
            var newColumn = new Column
            {
                Type = column.Type,
                Title = column.Alias,
                Index = column.Index,
                Randomly = column.Random,
                Optional = column.Optional,
                Description = column.Description,
                DefaultValue = @object,
                ColumnVisibility = column.ColumnVisibility,
            };

            if (withEvent)
            {
                newColumn.PropertyChanged += CellPropertyChanged;
            }

            return newColumn;
        }

        //-----

        internal object[] GetRow()
        {
            return _modifiedRow.ToArray();
        }

        //-----      
    }
}
