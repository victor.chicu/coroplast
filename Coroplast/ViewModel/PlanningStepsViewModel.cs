﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;
using Coroplast.Builders;
using Coroplast.Common;
using Coroplast.Objects;
using Coroplast.Property;
using Coroplast.Storage;
using Coroplast.Storage.Repository;
using Coroplast.Storage.Settings;
using GalaSoft.MvvmLight;
using log4net;

namespace Coroplast.ViewModel
{
    public class PlanningStepsViewModel : ViewModelBase
    {
        //-----

        private readonly StorageManager<ISchemaData> _schemaStorage;

        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //-----

        public PlanningStepsViewModel()
        {
            try
            {
                if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                {
                    return;
                }

                DataTable = new DataTable();

                _schemaStorage = StorageManagerCache<ISchemaData>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.SchemasDataCollectionName));

                Schema = GetDefaultSchema(_schemaStorage, DefaultSchemas.PlanningStepsSchemaName, BuildColumns());

                var filteredColumns = Schema.Columns.Where(x => x.Equals(ColumnVisibility.Default));

                CreateColumns(filteredColumns);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        //-----

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { Set(() => SelectedIndex, ref _selectedIndex, value); }
        }

        private int _selectedIndex;

        public DataTable DataTable
        {
            get { return _dataTable; }
            set
            {
                _dataTable = value;
                Set(() => DataTable, ref _dataTable, value);
            }
        }

        private DataTable _dataTable;

        public ISchemaData Schema { get; }

        //-----

        public void NewRow(IEnumerable<IColumnData> columns, object[] row)
        {
            var newRow = DataTable.NewRow();

            var list = columns.Select(column => row[column.Index]).ToList();

            newRow.RowError = row[0].ToString();
            newRow.ItemArray = list.ToArray();

            DataTable.Rows.Add(newRow);
        }

        public void DeleteRow(int index)
        {
            DataTable.Rows.RemoveAt(index);
        }

        public DataRow GetRow(int index)
        {
            if (index < 0 || index > DataTable.Rows.Count) throw new IndexOutOfRangeException("There is no such row");

            var row = DataTable.Rows[index];

            var rowId = Guid.Parse(row.RowError);

            row.SetTag(rowId);

            return row;
        }

        //-----

        private void CreateColumns(IEnumerable<IColumnData> columns)
        {
            foreach (var column in columns)
            {
                var type = Type.GetType(column.Type);

                if (type == null)
                {
                    throw new TypeLoadException($"Column type \"{column.Type}\" is not well formed.");
                }

                DataTable.Columns.Add(column.Alias, type);
            }
        }

        private ISchemaData QuerySchema(string schemaName)
        {
            var repository = new SchemaRepository(_schemaStorage);

            using (var cursor = repository.GetSchema(schemaName))
            {
                if (!cursor.MoveNext() || cursor.Current == null || !cursor.Current.Any()) return null;

                var latestVersion = cursor.Current.Max(x => x.Version);

                return cursor.Current.First(x => x.Version.Equals(latestVersion));
            }
        }

        private ISchemaData GetDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = QuerySchema(schemaName);

            return schema ?? CreateDefaultSchema(storageManager, schemaName, columns);
        }

        private ISchemaData CreateDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = new SchemaData(schemaName, 1, columns);

            var repository = new SchemaRepository(storageManager);

            repository.CreateSchemaAsync(schema);

            return schema;
        }

        //-----

        private static List<IColumnData> BuildColumns()
        {
            var columnBuilder = new ColumnBuilder();

            //System
            columnBuilder.Append("_id", "ID", "", typeof(Guid).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);
            columnBuilder.Append("_AT", "_AT", "", typeof(DateTime).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);
            columnBuilder.Append("_STATE", "_STATE", "", typeof(int).FullName, (int) PlanningState.NotPlanned, ColumnVisibility.Hidden);    
              
            columnBuilder.Append(PlanningStepsColumns.PartNumber, PlanningStepsColumns.PartNumber, $"Enter \"{PlanningStepsColumns.PartNumber}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(PlanningStepsColumns.SapNumber, PlanningStepsColumns.SapNumber, $"Enter \"{PlanningStepsColumns.SapNumber}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(PlanningStepsColumns.OrderId, PlanningStepsColumns.OrderId, $"Enter \"{PlanningStepsColumns.OrderId}\" here", typeof(long).FullName, null, ColumnVisibility.Default, OptionalType.Timestamp, true);
            columnBuilder.Append(PlanningStepsColumns.OrderQty, PlanningStepsColumns.OrderQty, $"Enter \"{PlanningStepsColumns.OrderQty}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(PlanningStepsColumns.Group, PlanningStepsColumns.Group, $"Enter \"{PlanningStepsColumns.Group}\" here", typeof(string).FullName, ColumnVisibility.Default);

            return columnBuilder.Build();
        }

        //-----
    }
}