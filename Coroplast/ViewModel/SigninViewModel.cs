﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Coroplast.ViewModel
{
    public class SignInViewModel : ViewModelBase
    {
        //-----

        private string username;
        private string password;
        private RelayCommand signInCommand;

        //-----

        public SignInViewModel()
        {
            signInCommand = new RelayCommand(OnSignInCommand);
        }

        //-----

        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                Set(() => Username, ref username, value);
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                Set(() => Password, ref password, value);
            }
        }

        public RelayCommand SignInCommand
        {
            get
            {
                return signInCommand;
            }
            set
            {
                Set(() => SignInCommand, ref signInCommand, value);
            }
        }

        //-----
              
        private void OnSignInCommand()
        {

        }

        //-----
    }
}