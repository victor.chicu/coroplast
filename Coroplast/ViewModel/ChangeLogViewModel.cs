﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Coroplast.Builders;
using Coroplast.Common;
using Coroplast.Extensions;
using Coroplast.Objects;
using Coroplast.Property;
using Coroplast.Storage;
using Coroplast.Storage.Repository;
using Coroplast.Storage.Settings;
using GalaSoft.MvvmLight;
using log4net;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Coroplast.ViewModel
{
    public class ChangeLogViewModel : ViewModelBase
    {
        //-----

        private int _selectedIndex;

        private bool _sleepControlVisibility;

        private DataTable _dataTable;

        private readonly StorageManager<ISchemaData> _storageManager;

        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //-----

        public ChangeLogViewModel()
        {
            try
            {
                if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                {
                    return;
                }

                DataTable = new DataTable();

                _storageManager = StorageManagerCache<ISchemaData>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.SchemasDataCollectionName));

                Schema = GetDefaultSchema(_storageManager, DefaultSchemas.ChangeLogSchemaName, BuildColumns());

                var filteredColumns = Schema.Columns.Where(x => x.Equals(ColumnVisibility.Default));

                CreateColumns(filteredColumns);                
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        //-----

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { Set(() => SelectedIndex, ref _selectedIndex, value); }
        }

        public DataTable DataTable
        {
            get { return _dataTable; }
            set
            {
                _dataTable = value;
                Set(() => DataTable, ref _dataTable, value);
            }
        }

        public ISchemaData Schema { get; }

        public bool SleepControlVisibility
        {
            get { return _sleepControlVisibility; }
            set { Set(() => SleepControlVisibility, ref _sleepControlVisibility, value); }
        }

        //-----

        public void NewRow(object[] row)
        {
            var newRow = DataTable.NewRow();

            newRow.ItemArray = row;

            DataTable.Rows.Add(newRow);
        }

        public void DeleteRow(int index)
        {
            DataTable.Rows.RemoveAt(index);
        }

        public void LoadChangeLogAsync(DataRow dataRow)
        {
            DataTable.Clear();

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = true));

                        var repository = new ChangeLogRepository(StorageManagerCache<BsonDocument>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.ChangeLogDataCollectionName)));

                        var rowId = Guid.Parse(dataRow.RowError);

                        var sort = new BsonDocumentSortDefinition<BsonDocument>(new BsonDocument
                            {
                                    {"_AT", 1}
                            });

                        var documents = repository.Find(Builders<BsonDocument>.Filter.Eq("_id", rowId), new FindOptions<BsonDocument>
                            {
                                Sort = sort
                            });

                        if (documents.Any())
                        {
                            var filteredColumns = Schema.Columns.Where(x => x.Equals(ColumnVisibility.Default)).ToArray();

                            var rows = documents.ToRows(filteredColumns);

                            foreach (var row in rows)
                            {
                                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                                    {
                                        try
                                        {
                                            NewRow(row.ToArray());
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Error(ex.ToString());
#if DEBUG
                                            MessageBox.Show(ex.ToString());
#endif
                                        }
                                    }));

                            }
                        }

                        Thread.Sleep(500);

                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = false));
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.ToString());
#if DEBUG
                        MessageBox.Show(ex.ToString());
#endif
                    }
                }).ContinueWith(r =>
                {
                    try
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => SleepControlVisibility = false));
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.ToString());
#if DEBUG
                        MessageBox.Show(ex.ToString());
#endif
                    }
                });
        }

        public DataRow GetRow(int index)
        {
            if (index < 0 || index > DataTable.Rows.Count) throw new IndexOutOfRangeException("There is no such row");

            var row = DataTable.Rows[index];

            var rowId = Guid.Parse(row.RowError);

            row.SetTag(rowId);

            return row;
        }

        //-----

        private void CreateColumns(IEnumerable<IColumnData> columns)
        {
            foreach (var column in columns)
            {
                var type = Type.GetType(column.Type);

                if (type == null)
                {
                    throw new TypeLoadException($"Column type \"{column.Type}\" is not well formed.");
                }

                DataTable.Columns.Add(column.Alias, type);
            }
        }

        private ISchemaData QuerySchema(string schemaName)
        {
            var repository = new SchemaRepository(_storageManager);

            using (var cursor = repository.GetSchema(schemaName))
            {
                if (!cursor.MoveNext() || cursor.Current == null || !cursor.Current.Any()) return null;

                var latestVersion = cursor.Current.Max(x => x.Version);

                return cursor.Current.First(x => x.Version.Equals(latestVersion));
            }
        }

        private ISchemaData GetDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = QuerySchema(schemaName);

            return schema ?? CreateDefaultSchema(storageManager, schemaName, columns);
        }

        //-----

        private static ISchemaData CreateDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = new SchemaData(schemaName, 1, columns);

            var repository = new SchemaRepository(storageManager);

            repository.CreateSchemaAsync(schema);

            return schema;
        }

        private static List<IColumnData> BuildColumns()
        {
            var columnBuilder = new ColumnBuilder();

            //System
            columnBuilder.Append("_id", "ID", "", typeof(Guid).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);
            columnBuilder.Append("_AT", "_AT", "", typeof(DateTime).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);

            //Default
            columnBuilder.Append(WarehouseColumnNames.SapNumber, WarehouseColumnNames.SapNumber, $"Enter \"{WarehouseColumnNames.SapNumber}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(WarehouseColumnNames.Quantity, WarehouseColumnNames.Quantity, $"Enter \"{WarehouseColumnNames.Quantity}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(WarehouseColumnNames.InvoiceNr, WarehouseColumnNames.InvoiceNr, $"Enter \"{WarehouseColumnNames.InvoiceNr}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(WarehouseColumnNames.Location, WarehouseColumnNames.Location, $"Enter \"{WarehouseColumnNames.Location}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(WarehouseColumnNames.ProductionDate, WarehouseColumnNames.ProductionDate, $"Enter \"{WarehouseColumnNames.ProductionDate}\" here", typeof(DateTime).FullName, ColumnVisibility.Default);
            columnBuilder.Append(WarehouseColumnNames.PackingUnit, WarehouseColumnNames.PackingUnit, $"Enter \"{WarehouseColumnNames.PackingUnit}\" here", typeof(int).FullName, ColumnVisibility.Default);

            return columnBuilder.Build();
        }

        //-----
    }
}
