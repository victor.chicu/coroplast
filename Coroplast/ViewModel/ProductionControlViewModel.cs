﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;
using Coroplast.Builders;
using Coroplast.Common;
using Coroplast.Objects;
using Coroplast.Property;
using Coroplast.Storage;
using Coroplast.Storage.Repository;
using Coroplast.Storage.Settings;
using GalaSoft.MvvmLight;
using log4net;

namespace Coroplast.ViewModel
{
    public class ProductionControlViewModel : ViewModelBase
    {
        //-----

        private readonly StorageManager<ISchemaData> _schemaStorage;

        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //-----

        public ProductionControlViewModel()
        {
            try
            {
                if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                {
                    return;
                }

                DataTable = new DataTable();

                _schemaStorage = StorageManagerCache<ISchemaData>.Instance.GetCachedStorage(DefaultConnectionSettings.GetConnectionSettings(StorageCollections.SchemasDataCollectionName));

                Schema = GetDefaultSchema(_schemaStorage, DefaultSchemas.ProductionControlSchemaName, BuildColumns());

                var filteredColumns = Schema.Columns.Where(x => x.Equals(ColumnVisibility.Default));

                CreateColumns(filteredColumns);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        //-----

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { Set(() => SelectedIndex, ref _selectedIndex, value); }
        }
        private int _selectedIndex;

        public DataTable DataTable
        {
            get { return _dataTable; }
            set
            {
                _dataTable = value;
                Set(() => DataTable, ref _dataTable, value);
            }
        }
        private DataTable _dataTable;

        public ISchemaData Schema { get; }

        //-----

        public void NewRow(IEnumerable<IColumnData> columns, object[] row)
        {
            var newRow = DataTable.NewRow();

            var list = columns.Select(column => row[column.Index]).ToList();

            newRow.RowError = row[0].ToString();
            newRow.ItemArray = list.ToArray();

            DataTable.Rows.Add(newRow);
        }

        public void DeleteRow(int index)
        {
            DataTable.Rows.RemoveAt(index);
        }

        public DataRow GetRow(int index)
        {
            if (index < 0 || index > DataTable.Rows.Count) throw new IndexOutOfRangeException("There is no such row");

            var row = DataTable.Rows[index];

            var rowId = Guid.Parse(row.RowError);

            row.SetTag(rowId);

            return row;
        }

        //-----

        private void CreateColumns(IEnumerable<IColumnData> columns)
        {
            foreach (var column in columns)
            {
                var type = Type.GetType(column.Type);

                if (type == null)
                {
                    throw new TypeLoadException($"Column type \"{column.Type}\" is not well formed.");
                }

                DataTable.Columns.Add(column.Alias, type);
            }
        }

        private ISchemaData QuerySchema(string schemaName)
        {
            var repository = new SchemaRepository(_schemaStorage);

            using (var cursor = repository.GetSchema(schemaName))
            {
                if (!cursor.MoveNext() || cursor.Current == null || !cursor.Current.Any()) return null;

                return cursor.Current.First();
            }
        }

        private ISchemaData GetDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = QuerySchema(schemaName);

            return schema ?? CreateDefaultSchema(storageManager, schemaName, columns);
        }

        private ISchemaData CreateDefaultSchema(StorageManager<ISchemaData> storageManager, string schemaName, IList<IColumnData> columns)
        {
            var schema = new SchemaData(schemaName, 1, columns);

            var repository = new SchemaRepository(storageManager);

            repository.CreateSchemaAsync(schema);

            return schema;
        }

        private static List<IColumnData> BuildColumns()
        {
            var columnBuilder = new ColumnBuilder();

            columnBuilder.Append("_id", "ID", "", typeof(Guid).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);
            columnBuilder.Append("_AT", "_AT", "", typeof(DateTime).FullName, null, ColumnVisibility.Hidden, OptionalType.None, true);

            columnBuilder.Append(ProductionControlColumns.PartNumber, ProductionControlColumns.PartNumber, $"Enter \"{ProductionControlColumns.PartNumber}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.SapNumber, ProductionControlColumns.SapNumber, $"Enter \"{ProductionControlColumns.SapNumber}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.WorkingSteps, ProductionControlColumns.WorkingSteps, $"Enter \"{ProductionControlColumns.WorkingSteps}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.WireNumber, ProductionControlColumns.WireNumber, $"Enter \"{ProductionControlColumns.WireNumber}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.CST, ProductionControlColumns.CST, $"Enter \"{ProductionControlColumns.CST}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Material, ProductionControlColumns.Material, $"Enter \"{ProductionControlColumns.Material}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Unit, ProductionControlColumns.Unit, $"Enter \"{ProductionControlColumns.Unit}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.QTY, ProductionControlColumns.QTY, $"Enter \"{ProductionControlColumns.QTY}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.TextProcess, ProductionControlColumns.TextProcess, $"Enter \"{ProductionControlColumns.TextProcess}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.MinPer100, ProductionControlColumns.MinPer100, $"Enter \"{ProductionControlColumns.MinPer100}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Equipment, ProductionControlColumns.Equipment, $"Enter \"{ProductionControlColumns.Equipment}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Strip1, ProductionControlColumns.Strip1, $"Enter \"{ProductionControlColumns.Strip1}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Strip2, ProductionControlColumns.Strip2, $"Enter \"{ProductionControlColumns.Strip2}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Tool, ProductionControlColumns.Tool, $"Enter \"{ProductionControlColumns.Tool}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Side, ProductionControlColumns.Side, $"Enter \"{ProductionControlColumns.Side}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.VisualAid, ProductionControlColumns.VisualAid, $"Enter \"{ProductionControlColumns.VisualAid}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.CAV, ProductionControlColumns.CAV, $"Enter \"{ProductionControlColumns.CAV}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Wires1, ProductionControlColumns.Wires1, $"Enter \"{ProductionControlColumns.Wires1}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Wires2, ProductionControlColumns.Wires2, $"Enter \"{ProductionControlColumns.Wires2}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Wires3, ProductionControlColumns.Wires3, $"Enter \"{ProductionControlColumns.Wires3}\" here", typeof(int).FullName, ColumnVisibility.Default);

            columnBuilder.Append(ProductionControlColumns.OrderId, ProductionControlColumns.OrderId, $"Enter \"{ProductionControlColumns.OrderId}\" here", typeof(long).FullName, null, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.OrderQty, ProductionControlColumns.OrderQty, $"Enter \"{ProductionControlColumns.OrderQty}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.OrderStep, ProductionControlColumns.OrderStep, $"Enter \"{ProductionControlColumns.OrderStep}\" here", typeof(int).FullName, ColumnVisibility.Default);      
            columnBuilder.Append(ProductionControlColumns.Group, ProductionControlColumns.Group, $"Enter \"{ProductionControlColumns.Group}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.LotSize, ProductionControlColumns.LotSize, $"Enter \"{ProductionControlColumns.LotSize}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.LotName, ProductionControlColumns.LotName, $"Enter \"{ProductionControlColumns.LotName}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.MinTimePerLot, ProductionControlColumns.MinTimePerLot, $"Enter \"{ProductionControlColumns.MinTimePerLot}\" here", typeof(double).FullName, ColumnVisibility.Default);

            columnBuilder.Append(ProductionControlColumns.OperatorName, ProductionControlColumns.OperatorName, $"Enter \"{ProductionControlColumns.OperatorName}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.OperatorNumber, ProductionControlColumns.OperatorNumber, $"Enter \"{ProductionControlColumns.OperatorNumber}\" here", typeof(int).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Shift, ProductionControlColumns.Shift, $"Enter \"{ProductionControlColumns.Shift}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.TeamGroup, ProductionControlColumns.TeamGroup, $"Enter \"{ProductionControlColumns.TeamGroup}\" here", typeof(string).FullName, ColumnVisibility.Default);
            columnBuilder.Append(ProductionControlColumns.Date, ProductionControlColumns.Date, $"Enter \"{ProductionControlColumns.Date}\" here", typeof(DateTime).FullName, ColumnVisibility.Default);

            return columnBuilder.Build();
        }

        //-----      
    }
}
