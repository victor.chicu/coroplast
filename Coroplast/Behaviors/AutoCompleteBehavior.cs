﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Coroplast.Behaviors
{
    public static class AutoCompleteBehavior
    {
        private static TextChangedEventHandler onTextChanged = new TextChangedEventHandler(OnTextChanged);
        private static KeyEventHandler onKeyDown = new KeyEventHandler(OnPreviewKeyDown);
        private static bool _isSubscribed = false;

        /// <summary>
        /// The collection to search for matches from.
        /// </summary>
        public static readonly DependencyProperty AutoCompleteItemsSource = DependencyProperty.RegisterAttached("AutoCompleteItemsSource", typeof(IEnumerable<String>), typeof(AutoCompleteBehavior), new UIPropertyMetadata(null, OnAutoCompleteItemsSource));

        /// <summary>
        /// Whether or not to ignore case when searching for matches.
        /// </summary>
        public static readonly DependencyProperty AutoCompleteStringComparison = DependencyProperty.RegisterAttached("AutoCompleteStringComparison", typeof(StringComparison), typeof(AutoCompleteBehavior), new UIPropertyMetadata(StringComparison.Ordinal));

        #region Items Source

        public static IEnumerable<string> GetAutoCompleteItemsSource(DependencyObject obj)
        {
            var value = obj.GetValue(AutoCompleteItemsSource);
            return value as IEnumerable<string>;
        }

        public static void SetAutoCompleteItemsSource(DependencyObject obj, IEnumerable<String> value)
        {
            obj.SetValue(AutoCompleteItemsSource, value);
        }

        private static void OnAutoCompleteItemsSource(object sender, DependencyPropertyChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (sender == null)
                return;

            //If we're being removed, remove the callbacks
            if (e.NewValue == null && _isSubscribed)
            {
                tb.TextChanged -= onTextChanged;
                tb.PreviewKeyDown -= onKeyDown;
                _isSubscribed = false;
            }
            else if (!_isSubscribed)
            {
                //New source.  Add the callbacks
                tb.TextChanged += onTextChanged;
                tb.PreviewKeyDown += onKeyDown;
                _isSubscribed = true;
            }
        }

        #endregion

        #region String Comparison

        public static StringComparison GetAutoCompleteStringComparison(DependencyObject obj)
        {
            return (StringComparison) obj.GetValue(AutoCompleteStringComparison);
        }

        public static void SetAutoCompleteStringComparison(DependencyObject obj, StringComparison value)
        {
            obj.SetValue(AutoCompleteStringComparison, value);
        }

        #endregion


        /// <summary>
        /// Search for auto-completion suggestions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender == null)
                return;

            if (e.Changes.Any(change => change.RemovedLength > 0) && !e.Changes.Any(change => change.AddedLength > 0))
                return;

            var textBox = e.OriginalSource as TextBox;

            if (string.IsNullOrEmpty(textBox?.Text))
                return;

            var comparer = GetAutoCompleteStringComparison(textBox);

            var keywords = GetAutoCompleteItemsSource(textBox).ToArray();

            var match = keywords.Where(subValue =>
                {
                    var text = textBox.Text;

                    var words = text.Split(' ');

                    return subValue != null && words.Any(word => subValue.Length >= word.Length);
                }).FirstOrDefault(value =>
                {
                    var result = value.Substring(0, textBox.Text.Length).Equals(textBox.Text, comparer);
                    return result;
                });
            


//            var keywords = textBox.Text.Split(' ');
//
//            foreach (var keyword in keywords)
//            {
//                match = keywords.Where(subvalue => subvalue != null && subvalue.Length >= keyword.Length).FirstOrDefault(value => value.Substring(0, keyword.Length).Equals(keyword, comparer));
//
//                if (string.IsNullOrEmpty(match))
//                    return;            
//
//                textBox.TextChanged -= onTextChanged;
//                textBox.Text = match;
//                textBox.CaretIndex = keyword.Length;
//                textBox.SelectionStart = keyword.Length;
//                textBox.SelectionLength = match.Length - keyword.Length;
//                textBox.TextChanged += onTextChanged;
//            }                 
        }

        /// <summary>
        /// Used for moving the caret to the end of the suggested auto-completion text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            TextBox tb = e.OriginalSource as TextBox;
            if (tb == null)
                return;

            //If we pressed enter and if the selected text goes all the way to the end, move our caret position to the end
            if (tb.SelectionLength > 0 && (tb.SelectionStart + tb.SelectionLength == tb.Text.Length))
            {
                tb.SelectionStart = tb.CaretIndex = tb.Text.Length;
                tb.SelectionLength = 0;
            }
        }
    }
}