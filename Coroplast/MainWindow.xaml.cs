﻿using Coroplast.View;
using log4net.Config;
using System;
using System.Windows;

namespace Coroplast
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            XmlConfigurator.Configure();

            InitializeComponent();

            SignInWindow signIn = new SignInWindow();

            bool? result = signIn.ShowDialog();

            if (result.HasValue && !result.Value)
            {
                Application.Current.Shutdown(0);
            }        
        }
    }
}