﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Coroplast.Converters
{
    public class PlaceholderVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //No text
            if (value == null)
            {
                return Visibility.Visible;
            }

            //Is text 
            if (value is bool && ((bool)value == false))
            {
                return Visibility.Collapsed;
            }

            //No text
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
