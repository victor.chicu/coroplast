﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Coroplast.View
{
    /// <summary>
    /// Interaction logic for DataTableView.xaml
    /// </summary>
    public partial class DataTableView
    {
        public DataTableView()
        {
            InitializeComponent();
        }

        public int SelectedIndex
        {
            get
            {
                //
                return (int) GetValue(SelectedIndexProperty);
            }
            set
            {
                //
                SetValue(SelectedIndexProperty, value);
            }
        }

        public bool ScanningVisibility
        {
            get
            {
                //
                return (bool) GetValue(ScanningVisibilityProperty);
            }
            set
            {
                //
                SetValue(ScanningVisibilityProperty, value);
            }
        }

        public bool ChangeLogVisibility
        {
            get
            {
                //
                return (bool)GetValue(ChangeLogVisibilityProperty);
            }
            set
            {
                //
                SetValue(ChangeLogVisibilityProperty, value);
            }
        }

        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register("SelectedIndex", typeof(int), typeof(DataTableView), new FrameworkPropertyMetadata(null)
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

        public static readonly DependencyProperty ScanningVisibilityProperty = DependencyProperty.Register("ScanningVisibility", typeof(bool), typeof(DataTableView), new FrameworkPropertyMetadata(null)
        {
            BindsTwoWayByDefault = true,
            DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
        });

        public static readonly DependencyProperty ChangeLogVisibilityProperty = DependencyProperty.Register("ChangeLogVisibility", typeof(bool), typeof(DataTableView), new FrameworkPropertyMetadata(null)
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });       
    }
}