﻿using log4net;
using System.Reflection;

namespace Coroplast.View
{
    /// <summary>
    /// Interaction logic for ScanningWindow.xaml
    /// </summary>
    public partial class ScanningWindow
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public ScanningWindow()
        {
            InitializeComponent();
        }
    }
}
