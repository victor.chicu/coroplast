﻿using System.Data;

namespace Coroplast.Property
{
    public static class TagAttachedProperty
    {
        public static object Tag { get; set; }

        public static void SetTag(this DataRow row, object @object)
        {
            Tag = @object;
        }

        public static object GetTag(this DataRow row)
        {
            return Tag;
        }
    }
}
