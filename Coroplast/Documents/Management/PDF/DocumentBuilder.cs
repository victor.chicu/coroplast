﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using MongoDB.Bson;
using System.Collections.Generic;
using System.IO;
using System;
using Coroplast.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Coroplast.Pdf
{
    public class DocumentBuilder
    {
        //-----

        private string fileName;

        private PdfPTable table;

        private readonly Document document;
        private readonly PdfWriter pdfWriter;
        private readonly FileStream stream;
        private readonly Zen.Barcode.CodeQrBarcodeDraw qr;
        //-----

        public DocumentBuilder(string fileName)
        {
            qr = Zen.Barcode.BarcodeDrawFactory.CodeQr;
            this.fileName = fileName;
            stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            document = new Document(PageSize.A4, 5, 5, 5, 5);
            pdfWriter = PdfWriter.GetInstance(document, stream);
            document.Open();
        }

        //-----

        public DocumentBuilder AddCell(string phrase, float fontSize, BaseColor background, BaseColor foreground, int alignment, bool bold)
        {
            var font = FontFactory.GetFont(!bold ? FontFactory.HELVETICA : FontFactory.HELVETICA_BOLD, fontSize, foreground);
      
            PdfPCell cell = new PdfPCell(new Paragraph(phrase, font))
            {
                NoWrap = false,
                UseAscender = true,
                BackgroundColor = background,    
                VerticalAlignment = alignment,        
                HorizontalAlignment = alignment
            };

            table.AddCell(cell);

            return this;
        }

        public DocumentBuilder AddCell(PdfPCell cell)
        {
            table.AddCell(cell);
            return this;
        }

        public DocumentBuilder AddQrCode(Guid pid)
        { 
            System.Drawing.Image qrImage = qr.Draw(pid.ToString(), 50);

            byte[] imageBytes;

            using (var memoryStream = new MemoryStream())
            {
                qrImage.Save(memoryStream, ImageFormat.Jpeg);            
                imageBytes = memoryStream.ToArray();
            }

            iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(imageBytes);
            pdfImage.ScaleAbsolute(75, 75);     
            PdfPCell cell = new PdfPCell(pdfImage, false);
            cell.BackgroundColor = BaseColor.LIGHT_GRAY;
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);
            return this;
        }

        public DocumentBuilder BuildTable()
        {
            document.Add(table);
            return this;
        }    

        public DocumentBuilder NewTables(int count)
        {  
            table = new PdfPTable(count);
            return this;
        }

        public static Bitmap Resize(Bitmap imgPhoto, Size objSize, ImageFormat enuType)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;

            int destX = 0;
            int destY = 0;
            int destWidth = objSize.Width;
            int destHeight = objSize.Height;

            Bitmap bmPhoto;
            if (enuType == ImageFormat.Png)
                bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format32bppArgb);
            else if (enuType == ImageFormat.Gif)
                bmPhoto = new Bitmap(destWidth, destHeight); //PixelFormat.Format8bppIndexed should be the right value for a GIF, but will throw an error with some GIF images so it's not safe to specify.
            else
                bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

            //For some reason the resolution properties will be 96, even when the source image is different, so this matching does not appear to be reliable.
            //bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            //If you want to override the default 96dpi resolution do it here
            //bmPhoto.SetResolution(72, 72);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.DrawImage(imgPhoto,
                new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
                new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        //-----

        internal void Store()
        {
            using (stream)
            {
                document.Close();
                pdfWriter.Close();
            }
        }

        internal void AddParagraph(Paragraph paragraph)
        {
            document.Add(paragraph);
        }

        

        //-----
    }
}
