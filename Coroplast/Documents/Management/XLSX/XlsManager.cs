﻿using Coroplast.Objects;
using MongoDB.Bson;
using OfficeOpenXml;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Data;
using Coroplast.Common;
using System.Globalization;

namespace Coroplast.XLSX
{
    public class XlsManager
    {
        //-----

        #region XLS MAP

        private readonly string[] xlsMap = {
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z",
                "AA",
                "AB",
                "AC",
                "AD",
                "AE",
                "AF",
                "AG",
                "AH",
                "AI",
                "AJ",
                "AK",
                "AL",
                "AM",
                "AN",
                "AO",
                "AP",
                "AQ",
                "AR",
                "AS",
                "AT",
                "AU",
                "AV",
                "AW",
                "AX",
                "AY",
            };

        #endregion

        //-----

        private readonly string filePath;

        //-----

        public XlsManager(string filePath)
        {
            this.filePath = filePath;
        }

        //-----

        public List<object[]> Import(IColumnData[] columns)
        {
            FileInfo fileInfo = new FileInfo(filePath);

            List<object[]> rows = new List<object[]>();

            using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
            {
                using (var stream = File.Open(filePath, FileMode.Open))
                {
                    excelPackage.Load(stream);

                    var worksheet = excelPackage.Workbook.Worksheets[1];
               
                    var hiddenColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Hidden));

                    var startIndexCell = hiddenColumns.Count();

                    var defaultColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

                    for (var rowIndex = 1; rowIndex < worksheet.Dimension.Rows; rowIndex++)
                    {
                        object[] fullRow = new object[columns.Length];

                        //Set default values for hidden cells
                        foreach (var column in hiddenColumns)
                        {
                            var type = Type.GetType(column.Type);

                            if (type == null)
                            {
                                throw new TypeLoadException(string.Format("Field type \"{0}\" is not well formed.", column.Type));
                            }

                            var typeCode = Type.GetTypeCode(type);

                            switch (typeCode)
                            {
                                case TypeCode.Int16:
                                case TypeCode.Int32:
                                case TypeCode.Int64:
                                    fullRow[column.Index] = (int)PlanningState.NotPlanned;
                                    break;

                                case TypeCode.Object:
                                    fullRow[column.Index] = Guid.NewGuid();
                                    break;

                                case TypeCode.DateTime:
                                    fullRow[column.Index] = DateTime.UtcNow;
                                    break;
                            }
                        }

                        var row = GetXlsRow(defaultColumns, worksheet, rowIndex);

                        var nextIndexCell = startIndexCell + 0;

                        foreach (var cell in row)
                        {
                            fullRow[nextIndexCell] = cell;
                            nextIndexCell++;
                        }

                        rows.Add(fullRow);
                    }
                }
            }

            return rows;
        }

        public void Export(string worksheetName, List<BsonDocument> documents, IColumnData[] columns)
        {
            FileInfo fileInfo = new FileInfo(filePath);

            using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
            {
                excelPackage.Workbook.Worksheets.Add(worksheetName);

                var worksheet = excelPackage.Workbook.Worksheets[1];
            
                worksheet.InsertColumn(1, columns.Length);

                var columnIndex = 1;

                var filteredColumns = columns.Where(x => x.ColumnVisibility.Equals(ColumnVisibility.Default));

                foreach (var column in filteredColumns)
                {
                    worksheet.Cells[1, columnIndex].Style.Font.Bold = true;
                    worksheet.Cells[1, columnIndex].Value = column.Name;
                    worksheet.Column(columnIndex).AutoFit();
                    columnIndex++;
                }

                var rowIndex = 2;

                foreach (var document in documents)
                {
                    columnIndex = 1;

                    foreach (var column in filteredColumns)
                    {
                        var type = Type.GetType(column.Type);

                        if (type == null)
                        {
                            throw new TypeLoadException(string.Format("Field type \"{0}\" is not well formed.", column.Type));
                        }
       
                        var typeCode = Type.GetTypeCode(type);

                        switch (typeCode)
                        {
                            case TypeCode.Empty:
                                {
                                    worksheet.Cells[rowIndex, columnIndex].Value = string.Empty;
                                }
                                break;
                            case TypeCode.Object:
                                if (type.IsAssignableFrom(typeof(Guid)))
                                {
                                    worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsGuid.ToString();
                                }
                                else
                                {
                                    throw new Exception("Object type is not assignable from GUID.");
                                }
                                break;
                            case TypeCode.DBNull:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.Boolean:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsBoolean.ToString();
                                break;
                            case TypeCode.Char:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.SByte:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.Byte:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.Int16:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.UInt16:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.Int32:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.UInt32:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt32.ToString();
                                break;
                            case TypeCode.Int64:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt64.ToString();
                                break;
                            case TypeCode.UInt64:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsInt64.ToString();
                                break;
                            case TypeCode.Single:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsDouble.ToString(CultureInfo.InvariantCulture);
                                break;
                            case TypeCode.Double:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsDouble.ToString(CultureInfo.InvariantCulture);
                                break;
                            case TypeCode.Decimal:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsDouble.ToString(CultureInfo.InvariantCulture);
                                break;
                            case TypeCode.DateTime:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].ToLocalTime().ToString(CultureInfo.InvariantCulture);
                                break;
                            case TypeCode.String:
                                worksheet.Cells[rowIndex, columnIndex].Value = document[column.Name].IsBsonNull ? "" : document[column.Name].AsString;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        columnIndex++;
                    }

                    rowIndex++;            
                }            

                excelPackage.Save();
            }
        }

        //-----

        private IEnumerable<object> GetXlsRow(IEnumerable<IColumnData> columns, ExcelWorksheet worksheet, int rdx)
        {
            var cdx = 0;

            var row = new List<object>();

            foreach (var column in columns)
            {
                var type = Type.GetType(column.Type);

                if (type == null)
                {
                    throw new TypeLoadException(string.Format("Field type \"{0}\" is not well formed.", column.Type));
                }

                var typeCode = Type.GetTypeCode(type);

                switch (typeCode)
                {
                    case TypeCode.Empty:
                        {
                            row.Add(null);
                        }
                        break;
                    case TypeCode.Object:
                        {
                            if (type.IsAssignableFrom(typeof(Guid)))
                            {
                                var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                                var value = cellRange.Value.ToString();

                                if (string.IsNullOrEmpty(value))
                                {
                                    row.Add(null);
                                }
                                else
                                {
                                    Guid guidValue;

                                    if (!Guid.TryParse(cellRange.Value.ToString(), out guidValue))
                                    {
                                        throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                    }

                                    row.Add(guidValue);
                                }                            
                            }
                        }
                        break;
                    case TypeCode.DBNull:
                        {
                            row.Add(null);
                        }
                        break;
                    case TypeCode.Boolean:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                bool booleanValue;

                                if (!bool.TryParse(cellRange.Value.ToString(), out booleanValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(booleanValue);
                            }                         
                        }
                        break;
                    case TypeCode.Char:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                char charValue;

                                if (!char.TryParse(cellRange.Value.ToString(), out charValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(charValue);
                            }
                        }
                        break;
                    case TypeCode.SByte:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                sbyte sbyteValue;

                                if (!sbyte.TryParse(cellRange.Value.ToString(), out sbyteValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(sbyteValue);
                            }
                        }
                        break;
                    case TypeCode.Byte:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                byte byteValue;

                                if (!byte.TryParse(cellRange.Value.ToString(), out byteValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(byteValue);
                            }
                        }
                        break;
                    case TypeCode.Int16:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                short shortValue;

                                if (!short.TryParse(cellRange.Value.ToString(), out shortValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(shortValue);
                            }
                        }
                        break;
                    case TypeCode.UInt16:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                ushort ushortValue;

                                if (!ushort.TryParse(cellRange.Value.ToString(), out ushortValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(ushortValue);
                            }
                        }
                        break;
                    case TypeCode.Int32:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                int intValue;

                                if (!int.TryParse(cellRange.Value.ToString(), out intValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(intValue);
                            }
                        }
                        break;
                    case TypeCode.UInt32:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                uint uintValue;

                                if (!uint.TryParse(cellRange.Value.ToString(), out uintValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(uintValue);
                            }
                        }
                        break;
                    case TypeCode.Int64:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                long longValue;

                                if (!long.TryParse(cellRange.Value.ToString(), out longValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(longValue);
                            }
                        }
                        break;
                    case TypeCode.UInt64:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            ulong ulongValue;

                            if (!ulong.TryParse(cellRange.Value.ToString(), out ulongValue))
                            {
                                throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                            }

                            row.Add(ulongValue);
                        }
                        break;
                    case TypeCode.Single:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                Single singleValue;

                                if (!Single.TryParse(cellRange.Value.ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out singleValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(singleValue);
                            }
                        }
                        break;
                    case TypeCode.Double:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                double doubleValue;

                                if (!double.TryParse(cellRange.Value.ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out doubleValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(doubleValue);
                            }                          
                        }
                        break;
                    case TypeCode.Decimal:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                double doubleValue;

                                if (!double.TryParse(cellRange.Value.ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out doubleValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(new decimal(doubleValue));
                            }                       
                        }
                        break;
                    case TypeCode.DateTime:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];

                            var value = cellRange.Value.ToString();

                            if (string.IsNullOrEmpty(value))
                            {
                                row.Add(null);
                            }
                            else
                            {
                                var str = cellRange.Value.ToString() + ".0";

                                DateTime timeValue;

                                if (!DateTime.TryParse(str, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out timeValue))
                                {
                                    throw new Exception($"\"XLS\" document was not well formed for column \"{column.Name}\" of type \"{column.Type}\"");
                                }

                                row.Add(timeValue);
                            }                          
                        }
                        break;
                    case TypeCode.String:
                        {
                            var cellRange = worksheet.Cells[rdx + 1, cdx + 1];
                            row.Add(cellRange.Value.ToString());
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                cdx++;
            }

            return row;
        }

        //-----
    }
}
