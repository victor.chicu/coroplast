﻿using System.Windows.Controls;
using Coroplast.Common;

namespace Coroplast.Rules
{
    public class GenericValidationRule : ValidationRule
    {
        public ColumnVisibility ColumnVisibility { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            double result = 0.0;
            bool canConvert = double.TryParse(value as string, out result);
            return new ValidationResult(canConvert, "Not a valid double");
        }
    }
}
