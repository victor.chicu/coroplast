﻿namespace Coroplast.Common
{
    public enum WorkShift
    {
        DAY,
        NIGHT,
        DAYNIGHT,
    }
}
