﻿namespace Coroplast.Common
{
    public enum PlanningState
    {
        Error = 0,
        Planned = 1,
        NotPlanned = 2,
    }
}
