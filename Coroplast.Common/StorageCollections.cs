﻿namespace Coroplast.Common
{
    public class StorageCollections
    {
        public const string UserDataCollectionName = "UserData";
        public const string GroupDataCollectionName = "GroupData";
        public const string DataStoreCollectionName = "StoreData";
        public const string SchemasDataCollectionName = "SchemasData";
        public const string PlanningDataCollectonName = "PlanningData";
        public const string ScanningDataCollectionName = "ScanningData";
        public const string ChangeLogDataCollectionName = "ChangeLogData";
        public const string SettingsDataCollectionName = "SettingsData";
        public const string EmployeesDataCollectionName = "EmployeesData";
        public const string WarehouseDataCollectionName = "WarehouseData";
        public const string ProductionControlCollectionName = "ProductionData";
    }
}
