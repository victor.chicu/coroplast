﻿namespace Coroplast.Common
{
    public enum PermissionType
    {
        None = 0,
        CreateRow = 1,
        ReadRow = 2,
        UpdateRow = 3,
        DeleteRow = 4
    }
}
