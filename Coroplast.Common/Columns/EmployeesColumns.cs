﻿namespace Coroplast.Common
{
    // ReSharper disable once InconsistentNaming
    public class EmployeesColumns
    {
        public const string Name = "Name";
        public const string Surname = "Surname";
        public const string PhoneNumber = "PhoneNumber";
        public const string OperatorNumber = "OperatorNumber";
        public const string Group = "Group";
        public const string Shift = "Shift";
        public const string TeamGroup = "TeamGroup";
    }
}
