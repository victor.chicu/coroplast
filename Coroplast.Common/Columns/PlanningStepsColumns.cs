﻿namespace Coroplast.Common
{
    // ReSharper disable once InconsistentNaming
    public class PlanningStepsColumns
    {
        public const string PartNumber = "PartNumber";
        public const string SapNumber = "SapNumber";
        public const string OrderId = "OrderId";
        public const string OrderQty = "OrderQty";
        public const string Group = "Group";
    }
}
