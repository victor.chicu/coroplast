﻿namespace Coroplast.Common
{
    // ReSharper disable once InconsistentNaming
    public class ProductionControlColumns
    {
        public const string PartNumber = "PartNumber";
        public const string SapNumber = "SapNumber";
        public const string WorkingSteps = "WorkingSteps";
        public const string WireNumber = "WireNumber";
        public const string CST = "CST";
        public const string Material = "Material";
        public const string Unit = "Unit";
        public const string QTY = "QTY";
        public const string TextProcess = "TextProcess";
        public const string MinPer100 = "MinPer100";
        public const string Equipment = "Equipment";
        public const string Strip1 = "Strip1";
        public const string Strip2 = "Strip2";
        public const string Tool = "Tool";
        public const string Side = "Side";
        public const string VisualAid = "VisualAid";
        public const string CAV = "CAV";
        public const string Wires1 = "Wires1";
        public const string Wires2 = "Wires2";
        public const string Wires3 = "Wires3";
        public const string OrderId = "OrderId";
        public const string OrderStep = "OrderStep";
        public const string OrderQty = "OrderQty";
        public const string Group = "Group";
        public const string LotSize = "LotSize";
        public const string LotName = "LotName";
        public const string MinTimePerLot = "MinTimePerLot";
        public const string OperatorName = "OperatorName";
        public const string OperatorNumber = "OperatorNumber";
        public const string TeamGroup = "TeamGroup";
        public const string Shift = "Shift";
        public const string Date = "Date";
    }
}
