﻿namespace Coroplast.Common
{
    // ReSharper disable once InconsistentNaming
    public class WarehouseColumnNames
    {
        public const string SapNumber = "SapNumber";
        public const string Quantity = "Quantity";
        public const string InvoiceNr = "InvoiceNr";
        public const string Location = "Location";
        public const string ProductionDate = "ProductionDate";
        public const string PackingUnit = "PackingUnit";
    }
}
