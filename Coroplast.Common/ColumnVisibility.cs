﻿using System;

namespace Coroplast.Common
{
    [Flags]
    public enum ColumnVisibility
    {
        None = 0,
        Hidden = 1,
        Default = 2,
    }
}
