﻿using System;
using System.Collections.Generic;

namespace Coroplast.Objects
{
    public interface ISchemaData
    {
        Guid Id { get; }

        int Version { get; set; }

        string Name { get; set; }

        object DefaultValue { get; set; }

        IList<IColumnData> Columns { get; set; }
    }
}