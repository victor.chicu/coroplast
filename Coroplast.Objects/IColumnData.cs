﻿using Coroplast.Common;
using System;

namespace Coroplast.Objects
{
    public interface IColumnData
    {
        Guid Id { get; set; }

        int Index { get; set; }

        bool Random { get; set; }

        string Type { get; set; }

        string Name { get; set; }

        string Alias { get; set; }

        string Description { get; set; }

        object DefaultValue { get; set; }

        OptionalType Optional { get; set; }

        ColumnVisibility ColumnVisibility { get; set; }

        bool Equals(ColumnVisibility value);
    }
}