﻿using Coroplast.Common;
using System;

namespace Coroplast.Objects
{
    public class ColumnData : IColumnData
    {
        public Guid Id { get; set; }

        public int Index { get; set; }

        public bool Random { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Alias { get; set; }

        public string Description { get; set; }

        public object DefaultValue { get; set; }

        public OptionalType Optional { get; set; }

        public ColumnVisibility ColumnVisibility { get; set; }

        public static ColumnData Create(int index, string name, string alias, string description, string type, object defaultValue, ColumnVisibility columnVisibility, OptionalType optional, bool random)
        {
            return new ColumnData
                {
                    Id = Guid.NewGuid(),
                    Type = type,
                    Name = name,
                    Index = index,
                    Alias = alias,
                    Random = random,
                    Optional = optional,
                    Description = description,
                    DefaultValue = defaultValue,
                    ColumnVisibility = columnVisibility
                };
        }

        public bool Equals(ColumnVisibility value)
        {
            return ColumnVisibility.Equals(value);
        }
    }
}