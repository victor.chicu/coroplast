﻿using System;

namespace Coroplast.Objects
{
    public interface IProductionData
    {
        Guid Id { get; set; }
    }
}
