﻿namespace Coroplast.Objects
{
    public class UserData : IUserData
    {
        public string Name { get; set;}

        public string Email { get; set; }

        public string Password { get; set; }
    }
}
