﻿using System;
using System.Collections.Generic;

namespace Coroplast.Objects
{
    public class SchemaData : ISchemaData
    {
        public SchemaData()
        {
            Id = Guid.NewGuid();
        }

        public SchemaData(string schemaName, int version, IList<IColumnData> columns) : this()
        {
            Name = schemaName;
            Version = version;
            Columns = columns;
        }

        public Guid Id { get; }

        public int Version { get; set; }

        public string Name { get; set; }

        public object DefaultValue { get; set; }

        public IList<IColumnData> Columns { get; set; }
    }
}