﻿namespace Coroplast.Objects
{
    public interface IUserData
    {
        string Name { get; set; }

        string Email { get; set; }

        string Password { get; set; }
    }
}
