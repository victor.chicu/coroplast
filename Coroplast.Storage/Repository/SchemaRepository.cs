﻿using Coroplast.Objects;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Coroplast.Storage.Repository
{
    public class SchemaRepository
    {
        private const string SchemaFieldName = "Name";

        private readonly StorageManager<ISchemaData> _schemaDataManager;

        public SchemaRepository(StorageManager<ISchemaData> schemaDataManager)
        {
            _schemaDataManager = schemaDataManager;
        }
 
        public Task CreateSchemaAsync(ISchemaData schemaData)
        {
            var repository = _schemaDataManager.GetRepository();

            return repository.InsertOneAsync(schemaData);
        }

        public IAsyncCursor<ISchemaData> GetSchema(string schemaName)
        {
            var filter = Builders<ISchemaData>.Filter.Eq(SchemaFieldName, schemaName);

            var repository = _schemaDataManager.GetRepository();

            var cursor = repository.FindSync(filter);

            return cursor;
        }

        public Task<IAsyncCursor<ISchemaData>> GetSchemaAsync(string schemaName)
        {
            var filter = Builders<ISchemaData>.Filter.Eq(SchemaFieldName, schemaName);

            var repository = _schemaDataManager.GetRepository();

            return repository.FindAsync(filter);
        }
    }
}
