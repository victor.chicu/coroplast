﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coroplast.Common;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Coroplast.Storage.Repository
{
    public class WarehouseRepository
    {
        //-----

        private readonly StorageManager<BsonDocument> _storageManager;

        //-----

        public WarehouseRepository(StorageManager<BsonDocument> storageManager)
        {
            _storageManager = storageManager;
        }

        //-----

        public void InsertMany(IEnumerable<KeyValuePair<Guid, BsonDocument>> documents)
        {
            _storageManager.GetRepository().InsertMany(documents.Select(x => x.Value));
        }

        public void UpdateMany(IEnumerable<KeyValuePair<Guid, BsonDocument>> documents)
        {
            var repository = _storageManager.GetRepository();

            foreach (var document in documents)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", document.Key);

                var update = Builders<BsonDocument>.Update.Set("_id", document.Key);

                for (var index = 0; index < document.Value.ElementCount; index++)
                {
                    var element = document.Value.GetElement(index);

                    update = update.Set(element.Name, element.Value);
                }

                repository.UpdateOne(filter, update);
            }
        }

        public void DeleteMany(IEnumerable<KeyValuePair<Guid, BsonDocument>> documents)
        {
            var repository = _storageManager.GetRepository();

            foreach (var document in documents)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", document.Key);

                repository.DeleteOne(filter);
            }
        }

        public List<BsonDocument> FindAll(FindOptions<BsonDocument> findOptions = null)
        {
            var cursor = _storageManager.GetRepository().FindSync(FilterDefinition<BsonDocument>.Empty, findOptions);
            return cursor.ToList();
        }

        //-----

        public async Task<List<BsonDocument>> LoadAsync(FindOptions<BsonDocument> findOptions = null)
        {
            var cursor = await _storageManager.GetRepository().FindAsync(FilterDefinition<BsonDocument>.Empty, findOptions);
            return await cursor.ToListAsync();
        }

        //-----
    }
}
