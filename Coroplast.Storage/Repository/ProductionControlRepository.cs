﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Coroplast.Common;
using MongoDB.Driver.Linq;
using System.Diagnostics;
using System.Linq;

namespace Coroplast.Storage.Repository
{
    public class ProductionControlRepository
    {
        //-----

        private readonly StorageManager<BsonDocument> _storageManager;

        //-----

        public ProductionControlRepository(StorageManager<BsonDocument> storageManager)
        {
            _storageManager = storageManager;
        }

        //-----

        public void InsertMany(IEnumerable<BsonDocument> documents)
        {
            _storageManager.GetRepository().InsertMany(documents);
        }

        public void UpdateMany(IEnumerable<KeyValuePair<Guid, BsonDocument>> documents)
        {
            var repository = _storageManager.GetRepository();

            foreach (var document in documents)
            {

                var filter = Builders<BsonDocument>.Filter.Eq("_id", document.Key);

                var update = Builders<BsonDocument>.Update.Set("_id", document.Key);

                for (var index = 0; index < document.Value.ElementCount; index++)
                {
                    var element = document.Value.GetElement(index);

                    update = update.Set(element.Name, element.Value);
                }

                repository.UpdateOne(filter, update);
            }
        }

        public void DeleteMany(IEnumerable<KeyValuePair<Guid, BsonDocument>> documents)
        {
            var repository = _storageManager.GetRepository();

            foreach (var document in documents)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", document.Key);

                repository.DeleteOne(filter);
            }
        }

        public List<BsonDocument> FindAll(FindOptions<BsonDocument, BsonDocument> findOptions = null)
        {
            var cursor = _storageManager.GetRepository().FindSync(FilterDefinition<BsonDocument>.Empty, findOptions);

            return cursor.ToList();
        }

        //-----

        public async Task InsertManyAsync(IEnumerable<BsonDocument> productions, Action<string> trigger)
        {
            var repository = _storageManager.GetRepository();

            await repository.InsertManyAsync(productions);
        }

        public async Task<List<BsonDocument>> LoadAsync(FindOptions<BsonDocument, BsonDocument> findOptions = null)
        {
            var cursor = await _storageManager.GetRepository().FindAsync(FilterDefinition<BsonDocument>.Empty, findOptions);

            return await cursor.ToListAsync();
        }

        public long FindMaxLotName()
        {
            IMongoQueryable<BsonDocument> queryable = _storageManager.GetRepository().AsQueryable();

            var result = queryable.Any();

            if (result) {
                List<BsonDocument> documents = queryable.ToList();
                var max = documents.Select(x => long.Parse(x[ProductionControlColumns.LotName].AsString.Substring(1))).Max();
                return max + 1;
            }

            return 1;
        }
    

        //-----
    }
}
