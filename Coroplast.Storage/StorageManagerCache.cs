﻿using Coroplast.Objects;
using Coroplast.Storage.Settings;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Concurrent;
using MongoDB.Bson;
using MongoDB.Driver.Core.Clusters;

namespace Coroplast.Storage
{
    public class StorageManager
    {
        public static bool TestConnection(DefaultConnectionSettings settings, Action<string> trigger)
        {
            try
            {
                var mongoClient = new MongoClient(new MongoClientSettings
                    {
                        Server = new MongoServerAddress(settings.DatabaseHost),
                        ServerSelectionTimeout = TimeSpan.FromSeconds(10),
                        
                    });

                mongoClient.GetDatabase(settings.DatabaseName).RunCommandAsync((Command<BsonDocument>) "{ ping : 1 }").Wait();

                var state = mongoClient.Cluster.Description.State;

                return state == ClusterState.Connected;
            }
            catch (Exception ex)
            {
                trigger(ex.ToString());
            }

            return false;
        }

        public static void RegisterWellKnownTypes()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(UserData)))
            {
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IUserData, UserData>(BsonSerializer.LookupSerializer<UserData>()));
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(GroupData)))
            {
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IGroupData, GroupData>(BsonSerializer.LookupSerializer<GroupData>()));
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(SchemaData)))
            {
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ISchemaData, SchemaData>(BsonSerializer.LookupSerializer<SchemaData>()));
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(ColumnData)))
            {
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IColumnData, ColumnData>(BsonSerializer.LookupSerializer<ColumnData>()));
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(ProductionData)))
            {
                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IProductionData, ProductionData>(BsonSerializer.LookupSerializer<ProductionData>()));
            }
        }
    }

    public class StorageManager<T>
    {
        //-----

        private readonly IMongoCollection<T> _collection;

        //-----

        public StorageManager(string connectionString, string databaseName, string collectionName)
        {
            _collection = GetCollection(connectionString, databaseName, collectionName);
        }

        //-----

        public static StorageManager<T> GetCachedStorageManager(DefaultConnectionSettings connectionSettings)
        {
            var storageManager = StorageManagerCache<T>.Instance.GetCachedStorage(connectionSettings);

            return storageManager;
        }

        //-----

        internal IMongoCollection<T> GetRepository()
        {
            return _collection;
        }

        //-----

        private static IMongoCollection<T> GetCollection(string connectionString, string databaseName, string collectionName)
        {
            var mongoClient = new MongoClient(new MongoUrl(connectionString));

            var mongoDatabase = mongoClient.GetDatabase(databaseName, new MongoDatabaseSettings
            {
                GuidRepresentation = GuidRepresentation.CSharpLegacy
            });

            var collection = mongoDatabase.GetCollection<T>(collectionName, new MongoCollectionSettings());

            return collection;
        }

        //-----
    }

    public class StorageManagerCache<T>
    {
        //-----

        private static ConcurrentDictionary<Tuple<string, string, string>, StorageManager<T>> _collectionPool;

        //-----

        public static StorageManagerCache<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StorageManagerCache<T>();
                }

                if (_collectionPool == null)
                {
                    _collectionPool = new ConcurrentDictionary<Tuple<string, string, string>, StorageManager<T>>();
                }

                return _instance;
            }
        }
        private static StorageManagerCache<T> _instance;

        //-----

        public StorageManager<T> GetCachedStorage(DefaultConnectionSettings connection)
        {
            var key = Tuple.Create(connection.ConnectionString, connection.DatabaseName, connection.CollectionName);

            StorageManager<T> cachedManager;

            if (!_collectionPool.TryGetValue(key, out cachedManager))
            {
                var storageManager = new StorageManager<T>(connection.ConnectionString, connection.DatabaseName, connection.CollectionName);

                _collectionPool.TryAdd(key, storageManager);

                return storageManager;
            }
            else
            {
                return cachedManager;
            }
        }

        //-----
    }
}
