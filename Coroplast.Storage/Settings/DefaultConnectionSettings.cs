﻿namespace Coroplast.Storage.Settings
{
    public class DefaultConnectionSettings
    {
        public string DatabaseHost { get; private set; }

        public string DatabaseName { get; private set; }

        public string CollectionName { get; private set; }

        public string ConnectionString { get; private set; }     

        public static DefaultConnectionSettings GetConnectionSettings()
        {
            return new DefaultConnectionSettings
                {
                    DatabaseHost = Properties.Settings.Default.DatabaseHost,
                    DatabaseName = Properties.Settings.Default.DatabaseName,
                    ConnectionString = Properties.Settings.Default.ConnectionString
                };
        }

        public static DefaultConnectionSettings GetConnectionSettings(string collectionName)
        {
            return new DefaultConnectionSettings
            {
                DatabaseHost = Properties.Settings.Default.DatabaseHost,
                DatabaseName = Properties.Settings.Default.DatabaseName,
                CollectionName = collectionName,
                ConnectionString = Properties.Settings.Default.ConnectionString
            };
        }
    }
}
